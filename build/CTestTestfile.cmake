# CMake generated Testfile for 
# Source directory: /home/weitinglo/catkin_ws/src
# Build directory: /home/weitinglo/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("velodyne/velodyne")
subdirs("velodyne/velodyne_msgs")
subdirs("lrt_detect")
subdirs("transportation")
subdirs("velodyne/velodyne_driver")
subdirs("velodyne/velodyne_laserscan")
subdirs("velodyne/velodyne_pointcloud")
