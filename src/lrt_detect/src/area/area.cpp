#include"area.h"
#include <iostream>

Area :: Area()
{
    xmin =0;
    xmax =0;
    ymin =0;
    ymax =0;
    zmin =0;
    zmax =0;
}
Area :: Area(float xminInput, float xmaxInput, float yminInput, float ymaxInput ,float zminInput, float zmaxInput)
{
    xmin = xminInput;
    xmax = xmaxInput;
    ymin = yminInput;
    ymax = ymaxInput;
    zmin = zminInput;
    zmax = zmaxInput;
}
float Area :: getXmin()
{
    return xmin;
}
float Area :: getXmax()
{
    return xmax;
}
float Area :: getYmin()
{
    return ymin;
}
float Area :: getYmax()
{
    return ymax;
}
float Area :: getZmin()
{
    return zmin;
}
float Area :: getZmax()
{
    return zmax;
}
