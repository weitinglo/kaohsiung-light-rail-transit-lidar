#ifndef AREA_H
#define AREA_H
#include <iostream>
#include <vector>
#include <string>

class Area
{
  private:
    float xmin;
    float xmax;
    float ymin;
    float ymax;
    float zmin;
    float zmax;
  public:
      Area();
      Area(float xminInput, float xmaxInput, float yminInput, float ymaxInput ,float zminInput, float zmaxInput);
      float getXmin();
      float getXmax();    
      float getYmin();
      float getYmax();    
      float getZmin();     
      float getZmax();
};

#endif