/* \author Wei-Ting Lo */
#include "sensors/lidar.h"
#include "processPointClouds.h"
#include "processPointClouds.cpp"
#include <ros/ros.h>
#include <curl/curl.h>
// PCL
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/conversions.h>
#include <pcl/registration/ndt.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/octree/octree_pointcloud_changedetector.h>
#include <pcl/filters/approximate_voxel_grid.h>

#include <iostream>
#include <vector>
#include <thread>
#include <fstream>
#include <string>
#include <time.h>
#include "area/area.h"

//********** Detect Area Resolution********
float boxClusterTolerance = 2;
int boxMinSize = 0;
int boxMaxSize = 0;
float filterRes = 1;

//************ Entire Area Range *********
float minXArea = -100, maxXArea = 100, minYArea = -100, maxYArea = 10, minZArea = -5, maxZArea = 25;

//********** Detect Area Floor Level *********
float minZFloorAreaR = -1.5, maxZFloorAreaR = 1.5;
float restrictedColorR = 0.7, restrictedColorG = 0.7, restrictedColorB = 0.7;
float warningColorR = 0.5, warningColorG = 0.5, warningColorB = 0.5;
float minZFloorAreaRW = -1.5, maxZFloorAreaW = -1.5;
//****** Rotate Lidar input 1 ****
float tran_x = 0;
float tran_y = 0;
float tran_z = 0;
float rotate_x = 0;
float rotate_y = 0;
float rotate_z = 0;
//***** Mode *****
int mode = 1;
int viewFull = 1;
int enableDetect = 0;
int enableMsg = 1;
int enableDB = 1;
int enableView = 1;
int enablePCDSave = 1;
int avoidLRT = 0;
int checkShutDown = 20;
//**** DB ******
int updateDBtime = 10;
int updateDBcount = 20;
int updateRStart = 1;
int restictionArea[65] = {0};

int warningArea[10] = {0};
int resetCountStart = 1;
int resetTime = 10;
int resetCount = 10;

//************ Alarm Area *********
Area areaArray[65];
int plot = 0;
//*************Detect Train coming*****************
float xminD1A = 0, xmaxD1A = 0, yminD1A = 0, ymaxD1A = 0, zminD1A = 0, zmaxD1A = 0;
float xminD2A = 0, xmaxD2A = 0, yminD2A = 0, ymaxD2A = 0, zminD2A = 0, zmaxD2A = 0;
float xminD3A = 0, xmaxD3A = 0, yminD3A = 0, ymaxD3A = 0, zminD3A = 0, zmaxD3A = 0;
float xminD1D = 0, xmaxD1D = 0, yminD1D = 0, ymaxD1D = 0, zminD1D = 0, zmaxD1D = 0;
float xminD2D = 0, xmaxD2D = 0, yminD2D = 0, ymaxD2D = 0, zminD2D = 0, zmaxD2D = 0;
float xminD3D = 0, xmaxD3D = 0, yminD3D = 0, ymaxD3D = 0, zminD3D = 0, zmaxD3D = 0;
bool trainFromC13 = false;
bool trainFromC13Field = false;
bool trainFromC13Road = false;
bool trainFromC13End = false;

float xminD4A = 0, xmaxD4A = 0, yminD4A = 0, ymaxD4A = 0, zminD4A = 0, zmaxD4A = 0;
float xminD5A = 0, xmaxD5A = 0, yminD5A = 0, ymaxD5A = 0, zminD5A = 0, zmaxD5A = 0;
float xminD6A = 0, xmaxD6A = 0, yminD6A = 0, ymaxD6A = 0, zminD6A = 0, zmaxD6A = 0;
float xminD4D = 0, xmaxD4D = 0, yminD4D = 0, ymaxD4D = 0, zminD4D = 0, zmaxD4D = 0;
float xminD5D = 0, xmaxD5D = 0, yminD5D = 0, ymaxD5D = 0, zminD5D = 0, zmaxD5D = 0;
float xminD6D = 0, xmaxD6D = 0, yminD6D = 0, ymaxD6D = 0, zminD6D = 0, zmaxD6D = 0;
bool trainFromC14 = false;
bool trainFromC14Field = false;
bool trainFromC14Road = false;
bool trainFromC14End = false;
bool avoidStart = false;

int d1AStatus = 0;
int d2AStatus = 0;
int d3AStatus = 0;
int d4AStatus = 0;
int d5AStatus = 0;
int d6AStatus = 0;
int d1DStatus = 0;
int d2DStatus = 0;
int d3DStatus = 0;
int d4DStatus = 0;
int d5DStatus = 0;
int d6DStatus = 0;
int shutAlarmCount = 100;

//**Restricted**
float xminR1 = 0, xmaxR1 = 0, yminR1 = 0, ymaxR1 = 0, zminR1 = 0, zmaxR1 = 0;
float xminR2 = 0, xmaxR2 = 0, yminR2 = 0, ymaxR2 = 0, zminR2 = 0, zmaxR2 = 0;
float xminR3 = 0, xmaxR3 = 0, yminR3 = 0, ymaxR3 = 0, zminR3 = 0, zmaxR3 = 0;
float xminR4 = 0, xmaxR4 = 0, yminR4 = 0, ymaxR4 = 0, zminR4 = 0, zmaxR4 = 0;
float xminR5 = 0, xmaxR5 = 0, yminR5 = 0, ymaxR5 = 0, zminR5 = 0, zmaxR5 = 0;
float xminR6 = 0, xmaxR6 = 0, yminR6 = 0, ymaxR6 = 0, zminR6 = 0, zmaxR6 = 0;
float xminR7 = 0, xmaxR7 = 0, yminR7 = 0, ymaxR7 = 0, zminR7 = 0, zmaxR7 = 0;
float xminR8 = 0, xmaxR8 = 0, yminR8 = 0, ymaxR8 = 0, zminR8 = 0, zmaxR8 = 0;
float xminR9 = 0, xmaxR9 = 0, yminR9 = 0, ymaxR9 = 0, zminR9 = 0, zmaxR9 = 0;
float xminR10 = 0, xmaxR10 = 0, yminR10 = 0, ymaxR10 = 0, zminR10 = 0, zmaxR10 = 0;
float xminR11 = 0, xmaxR11 = 0, yminR11 = 0, ymaxR11 = 0, zminR11 = 0, zmaxR11 = 0;
float xminR12 = 0, xmaxR12 = 0, yminR12 = 0, ymaxR12 = 0, zminR12 = 0, zmaxR12 = 0;
float xminR13 = 0, xmaxR13 = 0, yminR13 = 0, ymaxR13 = 0, zminR13 = 0, zmaxR13 = 0;
float xminR14 = 0, xmaxR14 = 0, yminR14 = 0, ymaxR14 = 0, zminR14 = 0, zmaxR14 = 0;
float xminR15 = 0, xmaxR15 = 0, yminR15 = 0, ymaxR15 = 0, zminR15 = 0, zmaxR15 = 0;
float xminR16 = 0, xmaxR16 = 0, yminR16 = 0, ymaxR16 = 0, zminR16 = 0, zmaxR16 = 0;
float xminR17 = 0, xmaxR17 = 0, yminR17 = 0, ymaxR17 = 0, zminR17 = 0, zmaxR17 = 0;
float xminR18 = 0, xmaxR18 = 0, yminR18 = 0, ymaxR18 = 0, zminR18 = 0, zmaxR18 = 0;
float xminR19 = 0, xmaxR19 = 0, yminR19 = 0, ymaxR19 = 0, zminR19 = 0, zmaxR19 = 0;
float xminR20 = 0, xmaxR20 = 0, yminR20 = 0, ymaxR20 = 0, zminR20 = 0, zmaxR20 = 0;
float xminR21 = 0, xmaxR21 = 0, yminR21 = 0, ymaxR21 = 0, zminR21 = 0, zmaxR21 = 0;
float xminR22 = 0, xmaxR22 = 0, yminR22 = 0, ymaxR22 = 0, zminR22 = 0, zmaxR22 = 0;
float xminR23 = 0, xmaxR23 = 0, yminR23 = 0, ymaxR23 = 0, zminR23 = 0, zmaxR23 = 0;
float xminR24 = 0, xmaxR24 = 0, yminR24 = 0, ymaxR24 = 0, zminR24 = 0, zmaxR24 = 0;
float xminR25 = 0, xmaxR25 = 0, yminR25 = 0, ymaxR25 = 0, zminR25 = 0, zmaxR25 = 0;
float xminR26 = 0, xmaxR26 = 0, yminR26 = 0, ymaxR26 = 0, zminR26 = 0, zmaxR26 = 0;
float xminR27 = 0, xmaxR27 = 0, yminR27 = 0, ymaxR27 = 0, zminR27 = 0, zmaxR27 = 0;
float xminR28 = 0, xmaxR28 = 0, yminR28 = 0, ymaxR28 = 0, zminR28 = 0, zmaxR28 = 0;
float xminR29 = 0, xmaxR29 = 0, yminR29 = 0, ymaxR29 = 0, zminR29 = 0, zmaxR29 = 0;
float xminR30 = 0, xmaxR30 = 0, yminR30 = 0, ymaxR30 = 0, zminR30 = 0, zmaxR30 = 0;
float xminR31 = 0, xmaxR31 = 0, yminR31 = 0, ymaxR31 = 0, zminR31 = 0, zmaxR31 = 0;
float xminR32 = 0, xmaxR32 = 0, yminR32 = 0, ymaxR32 = 0, zminR32 = 0, zmaxR32 = 0;
float xminR33 = 0, xmaxR33 = 0, yminR33 = 0, ymaxR33 = 0, zminR33 = 0, zmaxR33 = 0;
float xminR34 = 0, xmaxR34 = 0, yminR34 = 0, ymaxR34 = 0, zminR34 = 0, zmaxR34 = 0;
float xminR35 = 0, xmaxR35 = 0, yminR35 = 0, ymaxR35 = 0, zminR35 = 0, zmaxR35 = 0;
float xminR36 = 0, xmaxR36 = 0, yminR36 = 0, ymaxR36 = 0, zminR36 = 0, zmaxR36 = 0;
float xminR37 = 0, xmaxR37 = 0, yminR37 = 0, ymaxR37 = 0, zminR37 = 0, zmaxR37 = 0;
float xminR38 = 0, xmaxR38 = 0, yminR38 = 0, ymaxR38 = 0, zminR38 = 0, zmaxR38 = 0;
float xminR39 = 0, xmaxR39 = 0, yminR39 = 0, ymaxR39 = 0, zminR39 = 0, zmaxR39 = 0;
float xminR40 = 0, xmaxR40 = 0, yminR40 = 0, ymaxR40 = 0, zminR40 = 0, zmaxR40 = 0;
float xminR41 = 0, xmaxR41 = 0, yminR41 = 0, ymaxR41 = 0, zminR41 = 0, zmaxR41 = 0;
float xminR42 = 0, xmaxR42 = 0, yminR42 = 0, ymaxR42 = 0, zminR42 = 0, zmaxR42 = 0;
float xminR43 = 0, xmaxR43 = 0, yminR43 = 0, ymaxR43 = 0, zminR43 = 0, zmaxR43 = 0;
float xminR44 = 0, xmaxR44 = 0, yminR44 = 0, ymaxR44 = 0, zminR44 = 0, zmaxR44 = 0;
float xminR45 = 0, xmaxR45 = 0, yminR45 = 0, ymaxR45 = 0, zminR45 = 0, zmaxR45 = 0;
float xminR46 = 0, xmaxR46 = 0, yminR46 = 0, ymaxR46 = 0, zminR46 = 0, zmaxR46 = 0;
float xminR47 = 0, xmaxR47 = 0, yminR47 = 0, ymaxR47 = 0, zminR47 = 0, zmaxR47 = 0;
float xminR48 = 0, xmaxR48 = 0, yminR48 = 0, ymaxR48 = 0, zminR48 = 0, zmaxR48 = 0;
float xminR49 = 0, xmaxR49 = 0, yminR49 = 0, ymaxR49 = 0, zminR49 = 0, zmaxR49 = 0;
float xminR50 = 0, xmaxR50 = 0, yminR50 = 0, ymaxR50 = 0, zminR50 = 0, zmaxR50 = 0;
float xminR51 = 0, xmaxR51 = 0, yminR51 = 0, ymaxR51 = 0, zminR51 = 0, zmaxR51 = 0;
float xminR52 = 0, xmaxR52 = 0, yminR52 = 0, ymaxR52 = 0, zminR52 = 0, zmaxR52 = 0;
float xminR53 = 0, xmaxR53 = 0, yminR53 = 0, ymaxR53 = 0, zminR53 = 0, zmaxR53 = 0;
float xminR54 = 0, xmaxR54 = 0, yminR54 = 0, ymaxR54 = 0, zminR54 = 0, zmaxR54 = 0;
float xminR55 = 0, xmaxR55 = 0, yminR55 = 0, ymaxR55 = 0, zminR55 = 0, zmaxR55 = 0;
float xminR56 = 0, xmaxR56 = 0, yminR56 = 0, ymaxR56 = 0, zminR56 = 0, zmaxR56 = 0;
float xminR57 = 0, xmaxR57 = 0, yminR57 = 0, ymaxR57 = 0, zminR57 = 0, zmaxR57 = 0;
float xminR58 = 0, xmaxR58 = 0, yminR58 = 0, ymaxR58 = 0, zminR58 = 0, zmaxR58 = 0;
float xminR59 = 0, xmaxR59 = 0, yminR59 = 0, ymaxR59 = 0, zminR59 = 0, zmaxR59 = 0;
float xminR60 = 0, xmaxR60 = 0, yminR60 = 0, ymaxR60 = 0, zminR60 = 0, zmaxR60 = 0;
float xminR61 = 0, xmaxR61 = 0, yminR61 = 0, ymaxR61 = 0, zminR61 = 0, zmaxR61 = 0;
float xminR62 = 0, xmaxR62 = 0, yminR62 = 0, ymaxR62 = 0, zminR62 = 0, zmaxR62 = 0;
float xminR63 = 0, xmaxR63 = 0, yminR63 = 0, ymaxR63 = 0, zminR63 = 0, zmaxR63 = 0;
float xminR64 = 0, xmaxR64 = 0, yminR64 = 0, ymaxR64 = 0, zminR64 = 0, zmaxR64 = 0;
float xminR65 = 0, xmaxR65 = 0, yminR65 = 0, ymaxR65 = 0, zminR65 = 0, zmaxR65 = 0;

pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
ProcessPointClouds<pcl::PointXYZI> *pointProcessorI = new ProcessPointClouds<pcl::PointXYZI>();

ros::Publisher pub_a;
ros::Publisher pub_b;
ros::Publisher pub_res;
pcl::PointCloud<pcl::PointXYZI>::Ptr clouda_ptr(new pcl::PointCloud<pcl::PointXYZI>);
pcl::PointCloud<pcl::PointXYZI>::Ptr clouda_ptr_trans(new pcl::PointCloud<pcl::PointXYZI>);
pcl::PointCloud<pcl::PointXYZI>::Ptr clouda_filtered(new pcl::PointCloud<pcl::PointXYZI>);

pcl::PointCloud<pcl::PointXYZI>::Ptr cloudb_ptr(new pcl::PointCloud<pcl::PointXYZI>);
pcl::PointCloud<pcl::PointXYZI>::Ptr cloudb_filtered(new pcl::PointCloud<pcl::PointXYZI>);

pcl::PointCloud<pcl::PointXYZI>::Ptr cloudc_ptr(new pcl::PointCloud<pcl::PointXYZI>);
pcl::PointCloud<pcl::PointXYZI>::Ptr obj_ptr(new pcl::PointCloud<pcl::PointXYZI>);
Eigen::Matrix4f init_guess;
Eigen::Matrix4f finalMatrix;
Eigen::Matrix4f manualMatrix;

CURL *curl;

int demo13=0;
int demo14=0;

std::vector<int> newPointIdxVector;
float bestScore = 10;
int count = 0;
int avoidAreaList[65] = {0};
int loadConfig()
{
  // std::ifstream cFile("/home/chiper/catkin_ws/src/lrt_read_pcd/src/config2.txt");
  std::ifstream cFile("/home/weitinglo/catkin_ws/src/lrt_detect/src/config2.txt");

  if (cFile.is_open())
  {
    std::string line;
    while (getline(cFile, line))
    {
      line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
      if (line[0] == '*' || line.empty())
        continue;
      auto delimiterPos = line.find("=");
      auto name = line.substr(0, delimiterPos);
      auto value = line.substr(delimiterPos + 1);
      std::cout << name << " " << value << '\n';
      if (name == "viewFull")
      {
        viewFull = atof(value.c_str());
      }
      else if (name == "enableMsg")
      {
        enableMsg = atof(value.c_str());
      }
      else if (name == "updateDBtime")
      {
        updateDBtime = atoi(value.c_str());
      }
      else if (name == "boxClusterTolerance")
      {
        boxClusterTolerance = atof(value.c_str());
      }
      else if (name == "boxMinSize")
      {
        boxMinSize = atoi(value.c_str());
      }
      else if (name == "boxMaxSize")
      {
        boxMaxSize = atoi(value.c_str());
      }
      else if (name == "filterRes")
      {
        filterRes = atof(value.c_str());
      }
      else if (name == "minXArea")
      {
        minXArea = atof(value.c_str());
      }
      else if (name == "maxXArea")
      {
        maxXArea = atof(value.c_str());
      }
      else if (name == "minYArea")
      {
        minYArea = atof(value.c_str());
      }
      else if (name == "maxYArea")
      {
        maxYArea = atof(value.c_str());
      }
      else if (name == "minZArea")
      {
        minZArea = atof(value.c_str());
      }
      else if (name == "maxZArea")
      {
        maxZArea = atof(value.c_str());
      }
      else if (name == "restrictedColorR")
      {
        restrictedColorR = atof(value.c_str());
      }
      else if (name == "restrictedColorG")
      {
        restrictedColorG = atof(value.c_str());
      }
      else if (name == "restrictedColorB")
      {
        restrictedColorB = atof(value.c_str());
      }
      else if (name == "warningColorR")
      {
        warningColorR = atof(value.c_str());
      }
      else if (name == "warningColorG")
      {
        warningColorG = atof(value.c_str());
      }
      else if (name == "warningColorB")
      {
        warningColorB = atof(value.c_str());
      }
      else if (name == "minZFloorAreaR")
      {
        minZFloorAreaR = atof(value.c_str());
      }
      else if (name == "maxZFloorAreaR")
      {
        maxZFloorAreaR = atof(value.c_str());
      }
      else if (name == "minZFloorAreaRW")
      {
        minZFloorAreaRW = atof(value.c_str());
      }
      else if (name == "maxZFloorAreaW")
      {
        maxZFloorAreaW = atof(value.c_str());
      }
      else if (name == "enableDetect")
      {
        enableDetect = atof(value.c_str());
      }
      else if (name == "enableView")
      {
        enableView = atof(value.c_str());
      }
      else if (name == "tran_x")
      {
        tran_x = atof(value.c_str());
      }
      else if (name == "tran_y")
      {
        tran_y = atof(value.c_str());
      }
      else if (name == "tran_z")
      {
        tran_z = atof(value.c_str());
      }
      else if (name == "rotate_x")
      {
        rotate_x = atof(value.c_str());
      }
      else if (name == "rotate_y")
      {
        rotate_y = atof(value.c_str());
      }
      else if (name == "rotate_z")
      {
        rotate_z = atof(value.c_str());
      }
      else if (name == "mode")
      {
        mode = atoi(value.c_str());
      }
      else if (name == "enableDB")
      {
        enableDB = atoi(value.c_str());
      }
      else if (name == "enablePCDSave")
      {
        enablePCDSave = atoi(value.c_str());
      }
      else if (name == "avoidLRT")
      {
        avoidLRT = atoi(value.c_str());
      }
      else if (name == "xminD1A")
      {
        xminD1A = atof(value.c_str());
      }
      else if (name == "xmaxD1A")
      {
        xmaxD1A = atof(value.c_str());
      }
      else if (name == "yminD1A")
      {
        yminD1A = atof(value.c_str());
      }
      else if (name == "ymaxD1A")
      {
        ymaxD1A = atof(value.c_str());
      }
      else if (name == "zminD1A")
      {
        zminD1A = atof(value.c_str());
      }
      else if (name == "zmaxD1A")
      {
        zmaxD1A = atof(value.c_str());
      }
      else if (name == "xminD2A")
      {
        xminD2A = atof(value.c_str());
      }
      else if (name == "xmaxD2A")
      {
        xmaxD2A = atof(value.c_str());
      }
      else if (name == "yminD2A")
      {
        yminD2A = atof(value.c_str());
      }
      else if (name == "ymaxD2A")
      {
        ymaxD2A = atof(value.c_str());
      }
      else if (name == "zminD2A")
      {
        zminD2A = atof(value.c_str());
      }
      else if (name == "zmaxD2A")
      {
        zmaxD2A = atof(value.c_str());
      }
      else if (name == "xminD3A")
      {
        xminD3A = atof(value.c_str());
      }
      else if (name == "xmaxD3A")
      {
        xmaxD3A = atof(value.c_str());
      }
      else if (name == "yminD3A")
      {
        yminD3A = atof(value.c_str());
      }
      else if (name == "ymaxD3A")
      {
        ymaxD3A = atof(value.c_str());
      }
      else if (name == "zminD3A")
      {
        zminD3A = atof(value.c_str());
      }
      else if (name == "zmaxD3A")
      {
        zmaxD3A = atof(value.c_str());
      }
      else if (name == "xminD4A")
      {
        xminD4A = atof(value.c_str());
      }
      else if (name == "xmaxD4A")
      {
        xmaxD4A = atof(value.c_str());
      }
      else if (name == "yminD4A")
      {
        yminD4A = atof(value.c_str());
      }
      else if (name == "ymaxD4A")
      {
        ymaxD4A = atof(value.c_str());
      }
      else if (name == "zminD4A")
      {
        zminD4A = atof(value.c_str());
      }
      else if (name == "zmaxD4A")
      {
        zmaxD4A = atof(value.c_str());
      }
      else if (name == "xminD5A")
      {
        xminD5A = atof(value.c_str());
      }
      else if (name == "xmaxD5A")
      {
        xmaxD5A = atof(value.c_str());
      }
      else if (name == "yminD5A")
      {
        yminD5A = atof(value.c_str());
      }
      else if (name == "ymaxD5A")
      {
        ymaxD5A = atof(value.c_str());
      }
      else if (name == "zminD5A")
      {
        zminD5A = atof(value.c_str());
      }
      else if (name == "zmaxD5A")
      {
        zmaxD5A = atof(value.c_str());
      }
      else if (name == "xminD6A")
      {
        xminD6A = atof(value.c_str());
      }
      else if (name == "xmaxD6A")
      {
        xmaxD6A = atof(value.c_str());
      }
      else if (name == "yminD6A")
      {
        yminD6A = atof(value.c_str());
      }
      else if (name == "ymaxD6A")
      {
        ymaxD6A = atof(value.c_str());
      }
      else if (name == "zminD6A")
      {
        zminD6A = atof(value.c_str());
      }
      else if (name == "zmaxD6A")
      {
        zmaxD6A = atof(value.c_str());
      }
      else if (name == "xminD1D")
      {
        xminD1D = atof(value.c_str());
      }
      else if (name == "xmaxD1D")
      {
        xmaxD1D = atof(value.c_str());
      }
      else if (name == "yminD1D")
      {
        yminD1D = atof(value.c_str());
      }
      else if (name == "ymaxD1D")
      {
        ymaxD1D = atof(value.c_str());
      }
      else if (name == "zminD1D")
      {
        zminD1D = atof(value.c_str());
      }
      else if (name == "zmaxD1D")
      {
        zmaxD1D = atof(value.c_str());
      }
      else if (name == "xminD2D")
      {
        xminD2D = atof(value.c_str());
      }
      else if (name == "xmaxD2D")
      {
        xmaxD2D = atof(value.c_str());
      }
      else if (name == "yminD2D")
      {
        yminD2D = atof(value.c_str());
      }
      else if (name == "ymaxD2D")
      {
        ymaxD2D = atof(value.c_str());
      }
      else if (name == "zminD2D")
      {
        zminD2D = atof(value.c_str());
      }
      else if (name == "zmaxD2D")
      {
        zmaxD2D = atof(value.c_str());
      }
      else if (name == "xminD3D")
      {
        xminD3D = atof(value.c_str());
      }
      else if (name == "xmaxD3D")
      {
        xmaxD3D = atof(value.c_str());
      }
      else if (name == "yminD3D")
      {
        yminD3D = atof(value.c_str());
      }
      else if (name == "ymaxD3D")
      {
        ymaxD3D = atof(value.c_str());
      }
      else if (name == "zminD3D")
      {
        zminD3D = atof(value.c_str());
      }
      else if (name == "zmaxD3D")
      {
        zmaxD3D = atof(value.c_str());
      }
      else if (name == "xminD4D")
      {
        xminD4D = atof(value.c_str());
      }
      else if (name == "xmaxD4D")
      {
        xmaxD4D = atof(value.c_str());
      }
      else if (name == "yminD4D")
      {
        yminD4D = atof(value.c_str());
      }
      else if (name == "ymaxD4D")
      {
        ymaxD4D = atof(value.c_str());
      }
      else if (name == "zminD4D")
      {
        zminD4D = atof(value.c_str());
      }
      else if (name == "zmaxD4D")
      {
        zmaxD4D = atof(value.c_str());
      }
      else if (name == "xminD5D")
      {
        xminD5D = atof(value.c_str());
      }
      else if (name == "xmaxD5D")
      {
        xmaxD5D = atof(value.c_str());
      }
      else if (name == "yminD5D")
      {
        yminD5D = atof(value.c_str());
      }
      else if (name == "ymaxD5D")
      {
        ymaxD5D = atof(value.c_str());
      }
      else if (name == "zminD5D")
      {
        zminD5D = atof(value.c_str());
      }
      else if (name == "zmaxD5D")
      {
        zmaxD5D = atof(value.c_str());
      }
      else if (name == "xminD6D")
      {
        xminD6D = atof(value.c_str());
      }
      else if (name == "xmaxD6D")
      {
        xmaxD6D = atof(value.c_str());
      }
      else if (name == "yminD6D")
      {
        yminD6D = atof(value.c_str());
      }
      else if (name == "ymaxD6D")
      {
        ymaxD6D = atof(value.c_str());
      }
      else if (name == "zminD6D")
      {
        zminD6D = atof(value.c_str());
      }
      else if (name == "zmaxD6D")
      {
        zmaxD6D = atof(value.c_str());
      }
      else if (name == "xminR1")
      {
        xminR1 = atof(value.c_str());
      }
      else if (name == "xmaxR1")
      {
        xmaxR1 = atof(value.c_str());
      }
      else if (name == "yminR1")
      {
        yminR1 = atof(value.c_str());
      }
      else if (name == "ymaxR1")
      {
        ymaxR1 = atof(value.c_str());
      }
      else if (name == "zminR1")
      {
        zminR1 = atof(value.c_str());
      }
      else if (name == "zmaxR1")
      {
        zmaxR1 = atof(value.c_str());
      }
      else if (name == "xminR2")
      {
        xminR2 = atof(value.c_str());
      }
      else if (name == "xmaxR2")
      {
        xmaxR2 = atof(value.c_str());
      }
      else if (name == "yminR2")
      {
        yminR2 = atof(value.c_str());
      }
      else if (name == "ymaxR2")
      {
        ymaxR2 = atof(value.c_str());
      }
      else if (name == "zminR2")
      {
        zminR2 = atof(value.c_str());
      }
      else if (name == "zmaxR2")
      {
        zmaxR2 = atof(value.c_str());
      }
      else if (name == "xminR3")
      {
        xminR3 = atof(value.c_str());
      }
      else if (name == "xmaxR3")
      {
        xmaxR3 = atof(value.c_str());
      }
      else if (name == "yminR3")
      {
        yminR3 = atof(value.c_str());
      }
      else if (name == "ymaxR3")
      {
        ymaxR3 = atof(value.c_str());
      }
      else if (name == "zminR3")
      {
        zminR3 = atof(value.c_str());
      }
      else if (name == "zmaxR3")
      {
        zmaxR3 = atof(value.c_str());
      }
      else if (name == "xminR4")
      {
        xminR4 = atof(value.c_str());
      }
      else if (name == "xmaxR4")
      {
        xmaxR4 = atof(value.c_str());
      }
      else if (name == "yminR4")
      {
        yminR4 = atof(value.c_str());
      }
      else if (name == "ymaxR4")
      {
        ymaxR4 = atof(value.c_str());
      }
      else if (name == "zminR4")
      {
        zminR4 = atof(value.c_str());
      }
      else if (name == "zmaxR4")
      {
        zmaxR4 = atof(value.c_str());
      }
      else if (name == "xminR5")
      {
        xminR5 = atof(value.c_str());
      }
      else if (name == "xmaxR5")
      {
        xmaxR5 = atof(value.c_str());
      }
      else if (name == "yminR5")
      {
        yminR5 = atof(value.c_str());
      }
      else if (name == "ymaxR5")
      {
        ymaxR5 = atof(value.c_str());
      }
      else if (name == "zminR5")
      {
        zminR5 = atof(value.c_str());
      }
      else if (name == "zmaxR5")
      {
        zmaxR5 = atof(value.c_str());
      }
      else if (name == "xminR6")
      {
        xminR6 = atof(value.c_str());
      }
      else if (name == "xmaxR6")
      {
        xmaxR6 = atof(value.c_str());
      }
      else if (name == "yminR6")
      {
        yminR6 = atof(value.c_str());
      }
      else if (name == "ymaxR6")
      {
        ymaxR6 = atof(value.c_str());
      }
      else if (name == "zminR6")
      {
        zminR6 = atof(value.c_str());
      }
      else if (name == "zmaxR6")
      {
        zmaxR6 = atof(value.c_str());
      }
      else if (name == "xminR7")
      {
        xminR7 = atof(value.c_str());
      }
      else if (name == "xmaxR7")
      {
        xmaxR7 = atof(value.c_str());
      }
      else if (name == "yminR7")
      {
        yminR7 = atof(value.c_str());
      }
      else if (name == "ymaxR7")
      {
        ymaxR7 = atof(value.c_str());
      }
      else if (name == "zminR7")
      {
        zminR7 = atof(value.c_str());
      }
      else if (name == "zmaxR7")
      {
        zmaxR7 = atof(value.c_str());
      }
      else if (name == "xminR8")
      {
        xminR8 = atof(value.c_str());
      }
      else if (name == "xmaxR8")
      {
        xmaxR8 = atof(value.c_str());
      }
      else if (name == "yminR8")
      {
        yminR8 = atof(value.c_str());
      }
      else if (name == "ymaxR8")
      {
        ymaxR8 = atof(value.c_str());
      }
      else if (name == "zminR8")
      {
        zminR8 = atof(value.c_str());
      }
      else if (name == "zmaxR8")
      {
        zmaxR8 = atof(value.c_str());
      }
      else if (name == "xminR9")
      {
        xminR9 = atof(value.c_str());
      }
      else if (name == "xmaxR9")
      {
        xmaxR9 = atof(value.c_str());
      }
      else if (name == "yminR9")
      {
        yminR9 = atof(value.c_str());
      }
      else if (name == "ymaxR9")
      {
        ymaxR9 = atof(value.c_str());
      }
      else if (name == "zminR9")
      {
        zminR9 = atof(value.c_str());
      }
      else if (name == "zmaxR9")
      {
        zmaxR9 = atof(value.c_str());
      }
      else if (name == "xminR10")
      {
        xminR10 = atof(value.c_str());
      }
      else if (name == "xmaxR10")
      {
        xmaxR10 = atof(value.c_str());
      }
      else if (name == "yminR10")
      {
        yminR10 = atof(value.c_str());
      }
      else if (name == "ymaxR10")
      {
        ymaxR10 = atof(value.c_str());
      }
      else if (name == "zminR10")
      {
        zminR10 = atof(value.c_str());
      }
      else if (name == "zmaxR10")
      {
        zmaxR10 = atof(value.c_str());
      }
      else if (name == "xminR11")
      {
        xminR11 = atof(value.c_str());
      }
      else if (name == "xmaxR11")
      {
        xmaxR11 = atof(value.c_str());
      }
      else if (name == "yminR11")
      {
        yminR11 = atof(value.c_str());
      }
      else if (name == "ymaxR11")
      {
        ymaxR11 = atof(value.c_str());
      }
      else if (name == "zminR11")
      {
        zminR11 = atof(value.c_str());
      }
      else if (name == "zmaxR11")
      {
        zmaxR11 = atof(value.c_str());
      }
      else if (name == "xminR12")
      {
        xminR12 = atof(value.c_str());
      }
      else if (name == "xmaxR12")
      {
        xmaxR12 = atof(value.c_str());
      }
      else if (name == "yminR12")
      {
        yminR12 = atof(value.c_str());
      }
      else if (name == "ymaxR12")
      {
        ymaxR12 = atof(value.c_str());
      }
      else if (name == "zminR12")
      {
        zminR12 = atof(value.c_str());
      }
      else if (name == "zmaxR12")
      {
        zmaxR12 = atof(value.c_str());
      }
      else if (name == "xminR13")
      {
        xminR13 = atof(value.c_str());
      }
      else if (name == "xmaxR13")
      {
        xmaxR13 = atof(value.c_str());
      }
      else if (name == "yminR13")
      {
        yminR13 = atof(value.c_str());
      }
      else if (name == "ymaxR13")
      {
        ymaxR13 = atof(value.c_str());
      }
      else if (name == "yminR13")
      {
        zminR13 = atof(value.c_str());
      }
      else if (name == "ymaxR13")
      {
        zmaxR13 = atof(value.c_str());
      }
      else if (name == "xminR14")
      {
        xminR14 = atof(value.c_str());
      }
      else if (name == "xmaxR14")
      {
        xmaxR14 = atof(value.c_str());
      }
      else if (name == "yminR14")
      {
        yminR14 = atof(value.c_str());
      }
      else if (name == "ymaxR14")
      {
        ymaxR14 = atof(value.c_str());
      }
      else if (name == "zminR14")
      {
        zminR14 = atof(value.c_str());
      }
      else if (name == "zmaxR14")
      {
        zmaxR14 = atof(value.c_str());
      }
      else if (name == "xminR15")
      {
        xminR15 = atof(value.c_str());
      }
      else if (name == "xmaxR15")
      {
        xmaxR15 = atof(value.c_str());
      }
      else if (name == "yminR15")
      {
        yminR15 = atof(value.c_str());
      }
      else if (name == "ymaxR15")
      {
        ymaxR15 = atof(value.c_str());
      }
      else if (name == "zminR15")
      {
        zminR15 = atof(value.c_str());
      }
      else if (name == "zmaxR15")
      {
        zmaxR15 = atof(value.c_str());
      }
      else if (name == "xminR16")
      {
        xminR16 = atof(value.c_str());
      }
      else if (name == "xmaxR16")
      {
        xmaxR16 = atof(value.c_str());
      }
      else if (name == "yminR16")
      {
        yminR16 = atof(value.c_str());
      }
      else if (name == "ymaxR16")
      {
        ymaxR16 = atof(value.c_str());
      }
      else if (name == "zminR16")
      {
        zminR16 = atof(value.c_str());
      }
      else if (name == "zmaxR16")
      {
        zmaxR16 = atof(value.c_str());
      }
      else if (name == "xminR17")
      {
        xminR17 = atof(value.c_str());
      }
      else if (name == "xmaxR17")
      {
        xmaxR17 = atof(value.c_str());
      }
      else if (name == "yminR17")
      {
        yminR17 = atof(value.c_str());
      }
      else if (name == "ymaxR17")
      {
        ymaxR17 = atof(value.c_str());
      }
      else if (name == "zminR17")
      {
        zminR17 = atof(value.c_str());
      }
      else if (name == "zmaxR17")
      {
        zmaxR17 = atof(value.c_str());
      }
      else if (name == "xminR18")
      {
        xminR18 = atof(value.c_str());
      }
      else if (name == "xmaxR18")
      {
        xmaxR18 = atof(value.c_str());
      }
      else if (name == "yminR18")
      {
        yminR18 = atof(value.c_str());
      }
      else if (name == "ymaxR18")
      {
        ymaxR18 = atof(value.c_str());
      }
      else if (name == "zminR18")
      {
        zminR18 = atof(value.c_str());
      }
      else if (name == "zmaxR18")
      {
        zmaxR18 = atof(value.c_str());
      }
      else if (name == "xminR19")
      {
        xminR19 = atof(value.c_str());
      }
      else if (name == "xmaxR19")
      {
        xmaxR19 = atof(value.c_str());
      }
      else if (name == "yminR19")
      {
        yminR19 = atof(value.c_str());
      }
      else if (name == "ymaxR19")
      {
        ymaxR19 = atof(value.c_str());
      }
      else if (name == "zminR19")
      {
        zminR19 = atof(value.c_str());
      }
      else if (name == "zmaxR19")
      {
        zmaxR19 = atof(value.c_str());
      }
      else if (name == "xminR20")
      {
        xminR20 = atof(value.c_str());
      }
      else if (name == "xmaxR20")
      {
        xmaxR20 = atof(value.c_str());
      }
      else if (name == "yminR20")
      {
        yminR20 = atof(value.c_str());
      }
      else if (name == "ymaxR20")
      {
        ymaxR20 = atof(value.c_str());
      }
      else if (name == "zminR20")
      {
        zminR20 = atof(value.c_str());
      }
      else if (name == "zmaxR20")
      {
        zmaxR20 = atof(value.c_str());
      }
      else if (name == "xminR21")
      {
        xminR21 = atof(value.c_str());
      }
      else if (name == "xmaxR21")
      {
        xmaxR21 = atof(value.c_str());
      }
      else if (name == "yminR21")
      {
        yminR21 = atof(value.c_str());
      }
      else if (name == "ymaxR21")
      {
        ymaxR21 = atof(value.c_str());
      }
      else if (name == "zminR21")
      {
        zminR21 = atof(value.c_str());
      }
      else if (name == "zmaxR21")
      {
        zmaxR21 = atof(value.c_str());
      }
      else if (name == "xminR22")
      {
        xminR22 = atof(value.c_str());
      }
      else if (name == "xmaxR22")
      {
        xmaxR22 = atof(value.c_str());
      }
      else if (name == "yminR22")
      {
        yminR22 = atof(value.c_str());
      }
      else if (name == "ymaxR22")
      {
        ymaxR22 = atof(value.c_str());
      }
      else if (name == "zminR22")
      {
        zminR22 = atof(value.c_str());
      }
      else if (name == "zmaxR22")
      {
        zmaxR22 = atof(value.c_str());
      }
      else if (name == "xminR23")
      {
        xminR23 = atof(value.c_str());
      }
      else if (name == "xmaxR23")
      {
        xmaxR23 = atof(value.c_str());
      }
      else if (name == "yminR23")
      {
        yminR23 = atof(value.c_str());
      }
      else if (name == "ymaxR23")
      {
        ymaxR23 = atof(value.c_str());
      }
      else if (name == "zminR23")
      {
        zminR23 = atof(value.c_str());
      }
      else if (name == "zmaxR23")
      {
        zmaxR23 = atof(value.c_str());
      }
      else if (name == "xminR24")
      {
        xminR24 = atof(value.c_str());
      }
      else if (name == "xmaxR24")
      {
        xmaxR24 = atof(value.c_str());
      }
      else if (name == "yminR24")
      {
        yminR24 = atof(value.c_str());
      }
      else if (name == "ymaxR24")
      {
        ymaxR24 = atof(value.c_str());
      }
      else if (name == "zminR24")
      {
        zminR24 = atof(value.c_str());
      }
      else if (name == "zmaxR24")
      {
        zmaxR24 = atof(value.c_str());
      }
      else if (name == "xminR25")
      {
        xminR25 = atof(value.c_str());
      }
      else if (name == "xmaxR25")
      {
        xmaxR25 = atof(value.c_str());
      }
      else if (name == "yminR25")
      {
        yminR25 = atof(value.c_str());
      }
      else if (name == "ymaxR25")
      {
        ymaxR25 = atof(value.c_str());
      }
      else if (name == "zminR25")
      {
        zminR25 = atof(value.c_str());
      }
      else if (name == "zmaxR25")
      {
        zmaxR25 = atof(value.c_str());
      }
      else if (name == "xminR26")
      {
        xminR26 = atof(value.c_str());
      }
      else if (name == "xmaxR26")
      {
        xmaxR26 = atof(value.c_str());
      }
      else if (name == "yminR26")
      {
        yminR26 = atof(value.c_str());
      }
      else if (name == "ymaxR26")
      {
        ymaxR26 = atof(value.c_str());
      }
      else if (name == "zminR26")
      {
        zminR26 = atof(value.c_str());
      }
      else if (name == "zmaxR26")
      {
        zmaxR26 = atof(value.c_str());
      }
      else if (name == "xminR27")
      {
        xminR27 = atof(value.c_str());
      }
      else if (name == "xmaxR27")
      {
        xmaxR27 = atof(value.c_str());
      }
      else if (name == "yminR27")
      {
        yminR27 = atof(value.c_str());
      }
      else if (name == "ymaxR27")
      {
        ymaxR27 = atof(value.c_str());
      }
      else if (name == "zminR27")
      {
        zminR27 = atof(value.c_str());
      }
      else if (name == "zmaxR27")
      {
        zmaxR27 = atof(value.c_str());
      }
      else if (name == "xminR28")
      {
        xminR28 = atof(value.c_str());
      }
      else if (name == "xmaxR28")
      {
        xmaxR28 = atof(value.c_str());
      }
      else if (name == "yminR28")
      {
        yminR28 = atof(value.c_str());
      }
      else if (name == "ymaxR28")
      {
        ymaxR28 = atof(value.c_str());
      }
      else if (name == "zminR28")
      {
        zminR28 = atof(value.c_str());
      }
      else if (name == "zmaxR28")
      {
        zmaxR28 = atof(value.c_str());
      }
      else if (name == "xminR29")
      {
        xminR29 = atof(value.c_str());
      }
      else if (name == "xmaxR29")
      {
        xmaxR29 = atof(value.c_str());
      }
      else if (name == "yminR29")
      {
        yminR29 = atof(value.c_str());
      }
      else if (name == "ymaxR29")
      {
        ymaxR29 = atof(value.c_str());
      }
      else if (name == "zminR29")
      {
        zminR29 = atof(value.c_str());
      }
      else if (name == "zmaxR29")
      {
        zmaxR29 = atof(value.c_str());
      }
      else if (name == "xminR30")
      {
        xminR30 = atof(value.c_str());
      }
      else if (name == "xmaxR30")
      {
        xmaxR30 = atof(value.c_str());
      }
      else if (name == "yminR30")
      {
        yminR30 = atof(value.c_str());
      }
      else if (name == "ymaxR30")
      {
        ymaxR30 = atof(value.c_str());
      }
      else if (name == "zminR30")
      {
        zminR30 = atof(value.c_str());
      }
      else if (name == "zmaxR30")
      {
        zmaxR30 = atof(value.c_str());
      }
      else if (name == "xminR31")
      {
        xminR31 = atof(value.c_str());
      }
      else if (name == "xmaxR31")
      {
        xmaxR31 = atof(value.c_str());
      }
      else if (name == "yminR31")
      {
        yminR31 = atof(value.c_str());
      }
      else if (name == "ymaxR31")
      {
        ymaxR31 = atof(value.c_str());
      }
      else if (name == "zminR31")
      {
        zminR31 = atof(value.c_str());
      }
      else if (name == "zmaxR31")
      {
        zmaxR31 = atof(value.c_str());
      }
      else if (name == "xminR32")
      {
        xminR32 = atof(value.c_str());
      }
      else if (name == "xmaxR32")
      {
        xmaxR32 = atof(value.c_str());
      }
      else if (name == "yminR32")
      {
        yminR32 = atof(value.c_str());
      }
      else if (name == "ymaxR32")
      {
        ymaxR32 = atof(value.c_str());
      }
      else if (name == "zminR32")
      {
        zminR32 = atof(value.c_str());
      }
      else if (name == "zmaxR32")
      {
        zmaxR32 = atof(value.c_str());
      }
      else if (name == "xminR33")
      {
        xminR33 = atof(value.c_str());
      }
      else if (name == "xmaxR33")
      {
        xmaxR33 = atof(value.c_str());
      }
      else if (name == "yminR33")
      {
        yminR33 = atof(value.c_str());
      }
      else if (name == "ymaxR33")
      {
        ymaxR33 = atof(value.c_str());
      }
      else if (name == "zminR33")
      {
        zminR33 = atof(value.c_str());
      }
      else if (name == "zmaxR33")
      {
        zmaxR33 = atof(value.c_str());
      }
      else if (name == "xminR34")
      {
        xminR34 = atof(value.c_str());
      }
      else if (name == "xmaxR34")
      {
        xmaxR34 = atof(value.c_str());
      }
      else if (name == "yminR34")
      {
        yminR34 = atof(value.c_str());
      }
      else if (name == "ymaxR34")
      {
        ymaxR34 = atof(value.c_str());
      }
      else if (name == "zminR34")
      {
        zminR34 = atof(value.c_str());
      }
      else if (name == "zmaxR34")
      {
        zmaxR34 = atof(value.c_str());
      }
      else if (name == "xminR35")
      {
        xminR35 = atof(value.c_str());
      }
      else if (name == "xmaxR35")
      {
        xmaxR35 = atof(value.c_str());
      }
      else if (name == "yminR35")
      {
        yminR35 = atof(value.c_str());
      }
      else if (name == "ymaxR35")
      {
        ymaxR35 = atof(value.c_str());
      }
      else if (name == "zminR35")
      {
        zminR35 = atof(value.c_str());
      }
      else if (name == "zmaxR35")
      {
        zmaxR35 = atof(value.c_str());
      }
      else if (name == "xminR36")
      {
        xminR36 = atof(value.c_str());
      }
      else if (name == "xmaxR36")
      {
        xmaxR36 = atof(value.c_str());
      }
      else if (name == "yminR36")
      {
        yminR36 = atof(value.c_str());
      }
      else if (name == "ymaxR36")
      {
        ymaxR36 = atof(value.c_str());
      }
      else if (name == "zminR36")
      {
        zminR36 = atof(value.c_str());
      }
      else if (name == "zmaxR36")
      {
        zmaxR36 = atof(value.c_str());
      }
      else if (name == "xminR37")
      {
        xminR37 = atof(value.c_str());
      }
      else if (name == "xmaxR37")
      {
        xmaxR37 = atof(value.c_str());
      }
      else if (name == "yminR37")
      {
        yminR37 = atof(value.c_str());
      }
      else if (name == "ymaxR37")
      {
        ymaxR37 = atof(value.c_str());
      }
      else if (name == "zminR37")
      {
        zminR37 = atof(value.c_str());
      }
      else if (name == "zmaxR37")
      {
        zmaxR37 = atof(value.c_str());
      }
      else if (name == "xminR38")
      {
        xminR38 = atof(value.c_str());
      }
      else if (name == "xmaxR38")
      {
        xmaxR38 = atof(value.c_str());
      }
      else if (name == "yminR38")
      {
        yminR38 = atof(value.c_str());
      }
      else if (name == "ymaxR38")
      {
        ymaxR38 = atof(value.c_str());
      }
      else if (name == "zminR38")
      {
        zminR38 = atof(value.c_str());
      }
      else if (name == "zmaxR38")
      {
        zmaxR38 = atof(value.c_str());
      }
      else if (name == "xminR39")
      {
        xminR39 = atof(value.c_str());
      }
      else if (name == "xmaxR39")
      {
        xmaxR39 = atof(value.c_str());
      }
      else if (name == "yminR39")
      {
        yminR39 = atof(value.c_str());
      }
      else if (name == "ymaxR39")
      {
        ymaxR39 = atof(value.c_str());
      }
      else if (name == "zminR39")
      {
        zminR39 = atof(value.c_str());
      }
      else if (name == "zmaxR39")
      {
        zmaxR39 = atof(value.c_str());
      }
      else if (name == "xminR40")
      {
        xminR40 = atof(value.c_str());
      }
      else if (name == "xmaxR40")
      {
        xmaxR40 = atof(value.c_str());
      }
      else if (name == "yminR40")
      {
        yminR40 = atof(value.c_str());
      }
      else if (name == "ymaxR40")
      {
        ymaxR40 = atof(value.c_str());
      }
      else if (name == "zminR40")
      {
        zminR40 = atof(value.c_str());
      }
      else if (name == "zmaxR40")
      {
        zmaxR40 = atof(value.c_str());
      }
      else if (name == "xminR41")
      {
        xminR41 = atof(value.c_str());
      }
      else if (name == "xmaxR41")
      {
        xmaxR41 = atof(value.c_str());
      }
      else if (name == "yminR41")
      {
        yminR41 = atof(value.c_str());
      }
      else if (name == "ymaxR41")
      {
        ymaxR41 = atof(value.c_str());
      }
      else if (name == "zminR41")
      {
        zminR41 = atof(value.c_str());
      }
      else if (name == "zmaxR41")
      {
        zmaxR41 = atof(value.c_str());
      }
      else if (name == "xminR42")
      {
        xminR42 = atof(value.c_str());
      }
      else if (name == "xmaxR42")
      {
        xmaxR42 = atof(value.c_str());
      }
      else if (name == "yminR42")
      {
        yminR42 = atof(value.c_str());
      }
      else if (name == "ymaxR42")
      {
        ymaxR42 = atof(value.c_str());
      }
      else if (name == "zminR42")
      {
        zminR42 = atof(value.c_str());
      }
      else if (name == "zmaxR42")
      {
        zmaxR42 = atof(value.c_str());
      }
      else if (name == "xminR43")
      {
        xminR43 = atof(value.c_str());
      }
      else if (name == "xmaxR43")
      {
        xmaxR43 = atof(value.c_str());
      }
      else if (name == "yminR43")
      {
        yminR43 = atof(value.c_str());
      }
      else if (name == "ymaxR43")
      {
        ymaxR43 = atof(value.c_str());
      }
      else if (name == "zminR43")
      {
        zminR43 = atof(value.c_str());
      }
      else if (name == "zmaxR43")
      {
        zmaxR43 = atof(value.c_str());
      }
      else if (name == "xminR44")
      {
        xminR44 = atof(value.c_str());
      }
      else if (name == "xmaxR44")
      {
        xmaxR44 = atof(value.c_str());
      }
      else if (name == "yminR44")
      {
        yminR44 = atof(value.c_str());
      }
      else if (name == "ymaxR44")
      {
        ymaxR44 = atof(value.c_str());
      }
      else if (name == "zminR44")
      {
        zminR44 = atof(value.c_str());
      }
      else if (name == "zmaxR44")
      {
        zmaxR44 = atof(value.c_str());
      }
      else if (name == "xminR45")
      {
        xminR45 = atof(value.c_str());
      }
      else if (name == "xmaxR45")
      {
        xmaxR45 = atof(value.c_str());
      }
      else if (name == "yminR45")
      {
        yminR45 = atof(value.c_str());
      }
      else if (name == "ymaxR45")
      {
        ymaxR45 = atof(value.c_str());
      }
      else if (name == "zminR45")
      {
        zminR45 = atof(value.c_str());
      }
      else if (name == "zmaxR45")
      {
        zmaxR45 = atof(value.c_str());
      }
      else if (name == "xminR46")
      {
        xminR46 = atof(value.c_str());
      }
      else if (name == "xmaxR46")
      {
        xmaxR46 = atof(value.c_str());
      }
      else if (name == "yminR46")
      {
        yminR46 = atof(value.c_str());
      }
      else if (name == "ymaxR46")
      {
        ymaxR46 = atof(value.c_str());
      }
      else if (name == "zminR46")
      {
        zminR46 = atof(value.c_str());
      }
      else if (name == "zmaxR46")
      {
        zmaxR46 = atof(value.c_str());
      }
      else if (name == "xminR47")
      {
        xminR47 = atof(value.c_str());
      }
      else if (name == "xmaxR47")
      {
        xmaxR47 = atof(value.c_str());
      }
      else if (name == "yminR47")
      {
        yminR47 = atof(value.c_str());
      }
      else if (name == "ymaxR47")
      {
        ymaxR47 = atof(value.c_str());
      }
      else if (name == "zminR47")
      {
        zminR47 = atof(value.c_str());
      }
      else if (name == "zmaxR47")
      {
        zmaxR47 = atof(value.c_str());
      }
      else if (name == "xminR48")
      {
        xminR48 = atof(value.c_str());
      }
      else if (name == "xmaxR48")
      {
        xmaxR48 = atof(value.c_str());
      }
      else if (name == "yminR48")
      {
        yminR48 = atof(value.c_str());
      }
      else if (name == "ymaxR48")
      {
        ymaxR48 = atof(value.c_str());
      }
      else if (name == "zminR48")
      {
        zminR48 = atof(value.c_str());
      }
      else if (name == "zmaxR48")
      {
        zmaxR48 = atof(value.c_str());
      }
      else if (name == "xminR49")
      {
        xminR49 = atof(value.c_str());
      }
      else if (name == "xmaxR49")
      {
        xmaxR49 = atof(value.c_str());
      }
      else if (name == "yminR49")
      {
        yminR49 = atof(value.c_str());
      }
      else if (name == "ymaxR49")
      {
        ymaxR49 = atof(value.c_str());
      }
      else if (name == "zminR49")
      {
        zminR49 = atof(value.c_str());
      }
      else if (name == "zmaxR49")
      {
        zmaxR49 = atof(value.c_str());
      }
      else if (name == "xminR50")
      {
        xminR50 = atof(value.c_str());
      }
      else if (name == "xmaxR50")
      {
        xmaxR50 = atof(value.c_str());
      }
      else if (name == "yminR50")
      {
        yminR50 = atof(value.c_str());
      }
      else if (name == "ymaxR50")
      {
        ymaxR50 = atof(value.c_str());
      }
      else if (name == "zminR50")
      {
        zminR50 = atof(value.c_str());
      }
      else if (name == "zmaxR50")
      {
        zmaxR50 = atof(value.c_str());
      }
      else if (name == "xminR51")
      {
        xminR51 = atof(value.c_str());
      }
      else if (name == "xmaxR51")
      {
        xmaxR51 = atof(value.c_str());
      }
      else if (name == "yminR51")
      {
        yminR51 = atof(value.c_str());
      }
      else if (name == "ymaxR51")
      {
        ymaxR51 = atof(value.c_str());
      }
      else if (name == "zminR51")
      {
        zminR51 = atof(value.c_str());
      }
      else if (name == "zmaxR51")
      {
        zmaxR51 = atof(value.c_str());
      }
      else if (name == "xminR52")
      {
        xminR52 = atof(value.c_str());
      }
      else if (name == "xmaxR52")
      {
        xmaxR52 = atof(value.c_str());
      }
      else if (name == "yminR52")
      {
        yminR52 = atof(value.c_str());
      }
      else if (name == "ymaxR52")
      {
        ymaxR52 = atof(value.c_str());
      }
      else if (name == "zminR52")
      {
        zminR52 = atof(value.c_str());
      }
      else if (name == "zmaxR52")
      {
        zmaxR52 = atof(value.c_str());
      }
      else if (name == "xminR53")
      {
        xminR53 = atof(value.c_str());
      }
      else if (name == "xmaxR53")
      {
        xmaxR53 = atof(value.c_str());
      }
      else if (name == "yminR53")
      {
        yminR53 = atof(value.c_str());
      }
      else if (name == "ymaxR53")
      {
        ymaxR53 = atof(value.c_str());
      }
      else if (name == "zminR53")
      {
        zminR53 = atof(value.c_str());
      }
      else if (name == "zmaxR53")
      {
        zmaxR53 = atof(value.c_str());
      }
      else if (name == "xminR54")
      {
        xminR54 = atof(value.c_str());
      }
      else if (name == "xmaxR54")
      {
        xmaxR54 = atof(value.c_str());
      }
      else if (name == "yminR54")
      {
        yminR54 = atof(value.c_str());
      }
      else if (name == "ymaxR54")
      {
        ymaxR54 = atof(value.c_str());
      }
      else if (name == "zminR54")
      {
        zminR54 = atof(value.c_str());
      }
      else if (name == "zmaxR54")
      {
        zmaxR54 = atof(value.c_str());
      }
      else if (name == "xminR55")
      {
        xminR55 = atof(value.c_str());
      }
      else if (name == "xmaxR55")
      {
        xmaxR55 = atof(value.c_str());
      }
      else if (name == "yminR55")
      {
        yminR55 = atof(value.c_str());
      }
      else if (name == "ymaxR55")
      {
        ymaxR55 = atof(value.c_str());
      }
      else if (name == "zminR55")
      {
        zminR55 = atof(value.c_str());
      }
      else if (name == "zmaxR55")
      {
        zmaxR55 = atof(value.c_str());
      }
      else if (name == "xminR56")
      {
        xminR56 = atof(value.c_str());
      }
      else if (name == "xmaxR56")
      {
        xmaxR56 = atof(value.c_str());
      }
      else if (name == "yminR56")
      {
        yminR56 = atof(value.c_str());
      }
      else if (name == "ymaxR56")
      {
        ymaxR56 = atof(value.c_str());
      }
      else if (name == "zminR56")
      {
        zminR56 = atof(value.c_str());
      }
      else if (name == "zmaxR56")
      {
        zmaxR56 = atof(value.c_str());
      }
      else if (name == "xminR57")
      {
        xminR57 = atof(value.c_str());
      }
      else if (name == "xmaxR57")
      {
        xmaxR57 = atof(value.c_str());
      }
      else if (name == "yminR57")
      {
        yminR57 = atof(value.c_str());
      }
      else if (name == "ymaxR57")
      {
        ymaxR57 = atof(value.c_str());
      }
      else if (name == "zminR57")
      {
        zminR57 = atof(value.c_str());
      }
      else if (name == "zmaxR57")
      {
        zmaxR57 = atof(value.c_str());
      }
      else if (name == "xminR58")
      {
        xminR58 = atof(value.c_str());
      }
      else if (name == "xmaxR58")
      {
        xmaxR58 = atof(value.c_str());
      }
      else if (name == "yminR58")
      {
        yminR58 = atof(value.c_str());
      }
      else if (name == "ymaxR58")
      {
        ymaxR58 = atof(value.c_str());
      }
      else if (name == "zminR58")
      {
        zminR58 = atof(value.c_str());
      }
      else if (name == "zmaxR58")
      {
        zmaxR58 = atof(value.c_str());
      }
      else if (name == "xminR59")
      {
        xminR59 = atof(value.c_str());
      }
      else if (name == "xmaxR59")
      {
        xmaxR59 = atof(value.c_str());
      }
      else if (name == "yminR59")
      {
        yminR59 = atof(value.c_str());
      }
      else if (name == "ymaxR59")
      {
        ymaxR59 = atof(value.c_str());
      }
      else if (name == "zminR59")
      {
        zminR59 = atof(value.c_str());
      }
      else if (name == "zmaxR59")
      {
        zmaxR59 = atof(value.c_str());
      }
      else if (name == "xminR60")
      {
        xminR60 = atof(value.c_str());
      }
      else if (name == "xmaxR60")
      {
        xmaxR60 = atof(value.c_str());
      }
      else if (name == "yminR60")
      {
        yminR60 = atof(value.c_str());
      }
      else if (name == "ymaxR60")
      {
        ymaxR60 = atof(value.c_str());
      }
      else if (name == "zminR60")
      {
        zminR60 = atof(value.c_str());
      }
      else if (name == "zmaxR60")
      {
        zmaxR60 = atof(value.c_str());
      }
      else if (name == "xminR61")
      {
        xminR61 = atof(value.c_str());
      }
      else if (name == "xmaxR61")
      {
        xmaxR61 = atof(value.c_str());
      }
      else if (name == "yminR61")
      {
        yminR61 = atof(value.c_str());
      }
      else if (name == "ymaxR61")
      {
        ymaxR61 = atof(value.c_str());
      }
      else if (name == "zminR61")
      {
        zminR61 = atof(value.c_str());
      }
      else if (name == "zmaxR61")
      {
        zmaxR61 = atof(value.c_str());
      }
      else if (name == "xminR62")
      {
        xminR62 = atof(value.c_str());
      }
      else if (name == "xmaxR62")
      {
        xmaxR62 = atof(value.c_str());
      }
      else if (name == "yminR62")
      {
        yminR62 = atof(value.c_str());
      }
      else if (name == "ymaxR62")
      {
        ymaxR62 = atof(value.c_str());
      }
      else if (name == "zminR62")
      {
        zminR62 = atof(value.c_str());
      }
      else if (name == "zmaxR62")
      {
        zmaxR62 = atof(value.c_str());
      }
      else if (name == "xminR63")
      {
        xminR63 = atof(value.c_str());
      }
      else if (name == "xmaxR63")
      {
        xmaxR63 = atof(value.c_str());
      }
      else if (name == "yminR63")
      {
        yminR63 = atof(value.c_str());
      }
      else if (name == "ymaxR63")
      {
        ymaxR63 = atof(value.c_str());
      }
      else if (name == "zminR63")
      {
        zminR63 = atof(value.c_str());
      }
      else if (name == "zmaxR63")
      {
        zmaxR63 = atof(value.c_str());
      }
      else if (name == "xminR64")
      {
        xminR64 = atof(value.c_str());
      }
      else if (name == "xmaxR64")
      {
        xmaxR64 = atof(value.c_str());
      }
      else if (name == "yminR64")
      {
        yminR64 = atof(value.c_str());
      }
      else if (name == "ymaxR64")
      {
        ymaxR64 = atof(value.c_str());
      }
      else if (name == "zminR64")
      {
        zminR64 = atof(value.c_str());
      }
      else if (name == "zmaxR64")
      {
        zmaxR64 = atof(value.c_str());
      }
      else if (name == "xminR65")
      {
        xminR65 = atof(value.c_str());
      }
      else if (name == "xmaxR65")
      {
        xmaxR65 = atof(value.c_str());
      }
      else if (name == "yminR65")
      {
        yminR65 = atof(value.c_str());
      }
      else if (name == "ymaxR65")
      {
        ymaxR65 = atof(value.c_str());
      }
      else if (name == "zminR65")
      {
        zminR65 = atof(value.c_str());
      }
      else if (name == "zmaxR65")
      {
        zmaxR65 = atof(value.c_str());
      }
      else if (name == "demo13")
      {
        demo13 = atof(value.c_str());
      }
      else if (name == "demo14")
      {
        demo14 = atof(value.c_str());
      }
    }
  }
  else
  {
    std::cerr << "Couldn't open config file for reading.\n";
  }
}

const std::string currentDate()
{
  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  std::ostringstream oss;
  oss << std::put_time(&tm, "%Y%m%d");
  return oss.str();
}
void dbUpdate(int valueArray[], int type, CURL *curl)
{
  // std::cout << "DB update "+std::to_string(type)  << std::endl;
  try
  {
    int areaNum = 65;
    CURLcode res;
    std::string url = "";
    if (curl)
    {

      areaNum = sizeof(areaArray) / sizeof(areaArray[0]);
      url = "https://kaohsiung-lrt.firebaseio.com/" + currentDate() + "/restriction.json";

      curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
      curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
      std::string strfield = "{ \"time\":{\".sv\":\"timestamp\"} ,";
      int value = 0;
      for (int i = 0; i < areaNum - 1; i++)
      {
        int rNumber = i + 1;
        if (valueArray[i] < 100 && valueArray[i] > 0)
          value = 1;
        else
          value = 0;
        strfield = strfield + "\"r" + std::to_string(rNumber) + "\":" + std::to_string(value) + ",";
      }
      // if(valueArray[areaNum-1]<100 || valueArray[areaNum-1]>2)
      //   value = 1;
      strfield = strfield + "\"r" + std::to_string(areaNum) + "\":" + std::to_string(value) + "}";
      char *cstr = new char[strfield.length() + 1];
      std::strcpy(cstr, strfield.c_str());
      curl_easy_setopt(curl, CURLOPT_POSTFIELDS, cstr);
      res = curl_easy_perform(curl);
      /* Check for errors */
      if (res != CURLE_OK)
      {
        curl_easy_cleanup(curl);
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
      }
    }
  }
  catch (const std::exception &e)
  {
    curl_easy_cleanup(curl);
    std::cerr << "Request failed, error: " << e.what() << '\n';
  }
}

pcl::PointCloud<pcl::PointXYZI>::Ptr getDetectAreaDangerous(const pcl::PointCloud<pcl::PointXYZI>::Ptr &inputCloud)
{
  pcl::PointCloud<pcl::PointXYZI>::Ptr detectedArea(new pcl::PointCloud<pcl::PointXYZI>);
  for (int i = 0; i < sizeof(areaArray) / sizeof(areaArray[0]); i++)
  {
    if (avoidAreaList[i] == 0)
    {
      pcl::PointCloud<pcl::PointXYZI>::Ptr filterCloudR = pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(areaArray[i].getXmin(), areaArray[i].getYmin(), areaArray[i].getZmin(), 1), Eigen::Vector4f(areaArray[i].getXmax(), areaArray[i].getYmax(), areaArray[i].getZmax(), 1));
      *detectedArea = *detectedArea + *filterCloudR;
    }
  }
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD1A, yminD1A, zminD1A, 1), Eigen::Vector4f(xmaxD1A, ymaxD1A, zmaxD1A, 1)));
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD2A, yminD2A, zminD2A, 1), Eigen::Vector4f(xmaxD2A, ymaxD2A, zmaxD2A, 1)));
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD3A, yminD3A, zminD3A, 1), Eigen::Vector4f(xmaxD3A, ymaxD3A, zmaxD3A, 1)));
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD4A, yminD4A, zminD4A, 1), Eigen::Vector4f(xmaxD4A, ymaxD4A, zmaxD4A, 1)));
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD5A, yminD5A, zminD5A, 1), Eigen::Vector4f(xmaxD5A, ymaxD5A, zmaxD5A, 1)));
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD6A, yminD6A, zminD6A, 1), Eigen::Vector4f(xmaxD6A, ymaxD6A, zmaxD6A, 1)));
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD1D, yminD1D, zminD1D, 1), Eigen::Vector4f(xmaxD1D, ymaxD1D, zmaxD1D, 1)));
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD2D, yminD2D, zminD2D, 1), Eigen::Vector4f(xmaxD2D, ymaxD2D, zmaxD2D, 1)));
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD3D, yminD3D, zminD3D, 1), Eigen::Vector4f(xmaxD3D, ymaxD3D, zmaxD3D, 1)));
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD4D, yminD4D, zminD4D, 1), Eigen::Vector4f(xmaxD4D, ymaxD4D, zmaxD4D, 1)));
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD5D, yminD5D, zminD5D, 1), Eigen::Vector4f(xmaxD5D, ymaxD5D, zmaxD5D, 1)));
  *detectedArea = *detectedArea + *(pointProcessorI->FilterCloud(inputCloud, filterRes, Eigen::Vector4f(xminD6D, yminD6D, zminD6D, 1), Eigen::Vector4f(xmaxD6D, ymaxD6D, zmaxD6D, 1)));

  return detectedArea;
}

void plotDetectAreaDangerous()
{
  for (int i = 0; i < sizeof(areaArray) / sizeof(areaArray[0]); i++)
  {
    if (avoidAreaList[i] == 0)
    {
      viewer->addCube(areaArray[i].getXmin(), areaArray[i].getXmax(), areaArray[i].getYmin(), areaArray[i].getYmax(), minZFloorAreaR, maxZFloorAreaR, restrictedColorR + 0.025 * i, restrictedColorG + 0.02 * i, restrictedColorB+ 0.02 * i, std::to_string(i), 0);
    }
  }
  viewer->addCube(xminD1A, xmaxD1A, yminD1A, ymaxD1A, minZFloorAreaR, maxZFloorAreaR, 0.2, 0.3, 0.4, "d1a", 0);
  viewer->addCube(xminD2A, xmaxD2A, yminD2A, ymaxD2A, minZFloorAreaR, maxZFloorAreaR, 0.3, 0.4, 0.5, "d2a", 0);
  viewer->addCube(xminD3A, xmaxD3A, yminD3A, ymaxD3A, minZFloorAreaR, maxZFloorAreaR, 0.4, 0.5, 0.6, "d3a", 0);
  viewer->addCube(xminD4A, xmaxD4A, yminD4A, ymaxD4A, minZFloorAreaR, maxZFloorAreaR, 0.4, 0.4, 0.5, "d4a", 0);
  viewer->addCube(xminD5A, xmaxD5A, yminD5A, ymaxD5A, minZFloorAreaR, maxZFloorAreaR, 0.5, 0.5, 0.6, "d5a", 0);
  viewer->addCube(xminD6A, xmaxD6A, yminD6A, ymaxD6A, minZFloorAreaR, maxZFloorAreaR, 0.6, 0.6, 0.7, "d6a", 0);
  viewer->addCube(xminD1D, xmaxD1D, yminD1D, ymaxD1D, minZFloorAreaR, maxZFloorAreaR, 0.2, 0.3, 0.4, "d1d", 0);
  viewer->addCube(xminD2D, xmaxD2D, yminD2D, ymaxD2D, minZFloorAreaR, maxZFloorAreaR, 0.3, 0.4, 0.5, "d2d", 0);
  viewer->addCube(xminD3D, xmaxD3D, yminD3D, ymaxD3D, minZFloorAreaR, maxZFloorAreaR, 0.4, 0.5, 0.6, "d3d", 0);
  viewer->addCube(xminD4D, xmaxD4D, yminD4D, ymaxD4D, minZFloorAreaR, maxZFloorAreaR, 0.4, 0.4, 0.5, "d4d", 0);
  viewer->addCube(xminD5D, xmaxD5D, yminD5D, ymaxD5D, minZFloorAreaR, maxZFloorAreaR, 0.5, 0.5, 0.6, "d5d", 0);
  viewer->addCube(xminD6D, xmaxD6D, yminD6D, ymaxD6D, minZFloorAreaR, maxZFloorAreaR, 0.6, 0.6, 0.7, "d6d", 0);
}

void restrictionWebUpdate(int areaNum)
{
  updateRStart = 1;
  resetCountStart = 1;
  restictionArea[areaNum - 1] = 1;
  if (enableMsg == 1)
  {
    std::cout << "************************Dangerous r" + std::to_string(areaNum) << std::endl;
  }
}

void plotBox(pcl::PointCloud<pcl::PointXYZI>::Ptr cluster, int clusterId)
{
  if (enableView == 1)
  {
    Color boxColor = Color(1, 0, 0);
    Box box = pointProcessorI->BoundingBox(cluster);
    renderBox(viewer, box, clusterId, boxColor);
  }
}

void detectWhole(pcl::PointXYZI minPoint, pcl::PointXYZI maxPoint)
{
  for (int i = 0; i < sizeof(areaArray) / sizeof(areaArray[0]); i++)
  {
    Area area = areaArray[i];
    if (((maxPoint.x <= area.getXmax() && maxPoint.x >= area.getXmin() && maxPoint.y <= area.getYmax() && maxPoint.y >= area.getYmin()) ||
         (maxPoint.x <= area.getXmax() && maxPoint.x >= area.getXmin() && minPoint.y <= area.getYmax() && minPoint.y >= area.getYmin()) ||
         (minPoint.x <= area.getXmax() && minPoint.x >= area.getXmin() && maxPoint.y <= area.getYmax() && maxPoint.y >= area.getYmin()) ||
         (minPoint.x <= area.getXmax() && minPoint.x >= area.getXmin() && minPoint.y <= area.getYmax() && minPoint.y >= area.getYmin())) &&
        maxPoint.z <= area.getZmax() && minPoint.z >= area.getZmin())
    {
      restrictionWebUpdate(i + 1);
      plot = 1;
    }
  }
}
bool doOverlap(float minX1, float maxX1, float minY1, float maxY1, float minX2, float maxX2, float minY2, float maxY2)
{
  // If one rectangle is on left side of other
  if (minX1 > maxX2 || minX2 > maxX1)
    return false;
  // If one rectangle is above other
  if (minY1 > maxY2 || minY2 > maxY1)
    return false;
  return true;
}

bool detectRange(pcl::PointXYZI minPoint, pcl::PointXYZI maxPoint, int border[], int borderSize, int detectMin, int detectMax, int avoidList[], int avoidSize, int includeList[], int includeSize)
{
  bool arrive = false;
  for (int i = 0; i < borderSize; i++)
  {
    Area area = areaArray[border[i] - 1];
    if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, area.getXmin(), area.getXmax(), area.getYmin(), area.getYmax()))
    {
      return true;
    }
  }
  for (int i = detectMin - 1; i <= detectMax; i++)
  {
    bool skip = false;
    //avoid
    for (int j = 0; j < avoidSize; j++)
    {
      if (i == avoidList[j] - 1)
      {
        skip = true;
        break;
      }
    }

    if (!skip)
    {
      Area area = areaArray[i];
      if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, area.getXmin(), area.getXmax(), area.getYmin(), area.getYmax()) && maxPoint.z <= area.getZmax() && minPoint.z >= area.getZmin())
      {
        restrictionWebUpdate(i + 1);
        plot = 1;
      }
    }
  }

  //scan include list
  for (int i = 0; i < includeSize; i++)
  {
    Area area = areaArray[includeList[i]-1];
    if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, area.getXmin(), area.getXmax(), area.getYmin(), area.getYmax()) && maxPoint.z <= area.getZmax() && minPoint.z >= area.getZmin())
    {
      restrictionWebUpdate(includeList[i] + 1);
      plot = 1;
    }
  }

  return arrive;
}
void resetDetectArea()
{
  for (int i = 0; i < 65; i++)
    avoidAreaList[i] = 0;
}
void avoidDetectArea()
{
  if (trainFromC13End)
  { //41,42,44
    avoidAreaList[7] = 1;
    avoidAreaList[9] = 1;
    avoidAreaList[11] = 1;
    avoidAreaList[13] = 1;
    avoidAreaList[15] = 1;
    avoidAreaList[17] = 1;
    avoidAreaList[19] = 1;
    avoidAreaList[21] = 1;
    avoidAreaList[23] = 1;
    avoidAreaList[25] = 1;
    avoidAreaList[27] = 1;
    avoidAreaList[29] = 1;
    avoidAreaList[30] = 1;
    avoidAreaList[31] = 1;
    avoidAreaList[32] = 1;
    avoidAreaList[33] = 1;
    avoidAreaList[34] = 1;
    avoidAreaList[35] = 1;
    avoidAreaList[36] = 1;
    avoidAreaList[37] = 1;
    avoidAreaList[38] = 1;
    avoidAreaList[40] = 1;
    avoidAreaList[41] = 1;
    avoidAreaList[43] = 1;
    avoidAreaList[45] = 1;
    avoidAreaList[47] = 1;
    avoidAreaList[49] = 1;
  }
  else if (trainFromC13Road)
  { //26,28
    avoidAreaList[1] = 1;
    avoidAreaList[3] = 1;
    avoidAreaList[5] = 1;
    avoidAreaList[7] = 1;
    avoidAreaList[9] = 1;
    avoidAreaList[11] = 1;
    avoidAreaList[13] = 1;
    avoidAreaList[15] = 1;
    avoidAreaList[17] = 1;
    avoidAreaList[19] = 1;
    avoidAreaList[21] = 1;
    avoidAreaList[23] = 1;
    avoidAreaList[25] = 1;
    avoidAreaList[27] = 1;
    avoidAreaList[29] = 1;
    avoidAreaList[51] = 1;
  }
  else if (trainFromC13Field)
  { //14
    avoidAreaList[1] = 1;
    avoidAreaList[3] = 1;
    avoidAreaList[5] = 1;
    avoidAreaList[7] = 1;
    avoidAreaList[9] = 1;
    avoidAreaList[11] = 1;
    avoidAreaList[13] = 1;
    avoidAreaList[15] = 1;
    avoidAreaList[17] = 1;
    avoidAreaList[19] = 1;
    avoidAreaList[21] = 1;
    avoidAreaList[51] = 1;
  }
  else if (avoidStart)
  { // not yet coming
    avoidAreaList[1] = 1;
    avoidAreaList[3] = 1;
    avoidAreaList[5] = 1;
    avoidAreaList[7] = 1;
    avoidAreaList[9] = 1;
  }

  if (trainFromC14End)
  { //13
    avoidAreaList[0] = 1;
    avoidAreaList[2] = 1;
    avoidAreaList[4] = 1;
    avoidAreaList[6] = 1;
    avoidAreaList[8] = 1;
    avoidAreaList[10] = 1;
    avoidAreaList[12] = 1;
    avoidAreaList[14] = 1;
    avoidAreaList[16] = 1;
    avoidAreaList[18] = 1;
    avoidAreaList[20] = 1;
    avoidAreaList[22] = 1;
    avoidAreaList[24] = 1;
    avoidAreaList[26] = 1;
    avoidAreaList[28] = 1;
    avoidAreaList[39] = 1;
    avoidAreaList[42] = 1;
    avoidAreaList[44] = 1;
    avoidAreaList[46] = 1;
    avoidAreaList[48] = 1;
  }
  else if (trainFromC14Road)
  { //23,25
    avoidAreaList[22] = 1;
    avoidAreaList[24] = 1;
    avoidAreaList[26] = 1;
    avoidAreaList[28] = 1;
    avoidAreaList[39] = 1;
    avoidAreaList[42] = 1;
    avoidAreaList[44] = 1;
    avoidAreaList[46] = 1;
    avoidAreaList[48] = 1;
  }
  else if (trainFromC14Field)
  { //40,43
    avoidAreaList[26] = 1;
    avoidAreaList[28] = 1;
    avoidAreaList[39] = 1;
    avoidAreaList[42] = 1;
    avoidAreaList[44] = 1;
    avoidAreaList[46] = 1;
    avoidAreaList[48] = 1;
  }
  else if (avoidStart)
  {
    avoidAreaList[46] = 1;
    avoidAreaList[48] = 1;
  }
}
void avoidLRTCheck(pcl::PointXYZI minPoint, pcl::PointXYZI maxPoint)
{
  //detect train
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD1A, xmaxD1A, yminD1A, ymaxD1A))
  {
    std::cout << "D1A" << std::endl;
    d1AStatus = 1;
  }
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD2A, xmaxD2A, yminD2A, ymaxD2A))
  {
    std::cout << "D2A" << std::endl;
    d2AStatus = 1;
  }
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD3A, xmaxD3A, yminD3A, ymaxD3A))
  {

    std::cout << "D3A" << std::endl;
    d3AStatus = 1;
  }
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD4A, xmaxD4A, yminD4A, ymaxD4A))
  {
    std::cout << "D4A" << std::endl;
    d4AStatus = 1;
  }
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD5A, xmaxD5A, yminD5A, ymaxD5A))
  {
    std::cout << "D5A" << std::endl;
    d5AStatus = 1;
  }
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD6A, xmaxD6A, yminD6A, ymaxD6A))
  {
    std::cout << "D6A" << std::endl;
    d6AStatus = 1;
  }
  //detect train
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD1D, xmaxD1D, yminD1D, ymaxD1D))
  {
    std::cout << "D1D" << std::endl;
    d1DStatus = 1;
  }
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD2D, xmaxD2D, yminD2D, ymaxD2D))
  {
    std::cout << "D2D" << std::endl;
    d2DStatus = 1;
  }
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD3D, xmaxD3D, yminD3D, ymaxD3D))
  {
    std::cout << "D3D" << std::endl;
    d3DStatus = 1;
  }
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD4D, xmaxD4D, yminD4D, ymaxD4D))
  {
    std::cout << "D4D" << std::endl;
    d4DStatus = 1;
  }
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD5D, xmaxD5D, yminD5D, ymaxD5D))
  {
    std::cout << "D5D" << std::endl;
    d5DStatus = 1;
  }
  if (doOverlap(minPoint.x, maxPoint.x, minPoint.y, maxPoint.y, xminD6D, xmaxD6D, yminD6D, ymaxD6D))
  {
    std::cout << "D6D" << std::endl;
    d6DStatus = 1;
  }

  //restiction

  //From C14
  if (d1AStatus + d2AStatus + d3AStatus >= 1)
  {
    trainFromC14 = true; // train is coming from A
  }
  if (d1DStatus + d2DStatus + d3DStatus >= 2)
  {
    trainFromC14 = false; // train is coming from A, arrived D
  }
  //From C13
  if (d4DStatus + d5DStatus + d6DStatus >= 1)
  {
    trainFromC13 = true; // train is coming from D
  }
  if (d4AStatus + d5AStatus + d6AStatus >= 2)
  {
    trainFromC13 = false; // train is coming from A, arrived D
  }

  if(demo13==1){
    trainFromC13=true;
  }
   if(demo14==1){
    trainFromC14=true;
  }

  resetDetectArea();
  avoidDetectArea();
  if (trainFromC13 && !trainFromC14)
  { // Only Train coming from C13
    std::cout << "From C13 Only" << std::endl;

    if (trainFromC13End)
    {
      std::cout << "s-4" << std::endl;
    }
    else if (trainFromC13Road)
    {
      int borderList[3] = {41, 42, 44};
      int avoidList[2] = {51, 52};
      int includeList[] = {};
      std::cout << "s-3" << std::endl;
      trainFromC13End = detectRange(minPoint, maxPoint, borderList, 3, 45, 50, avoidList, 2, includeList, 0);
    }
    else if (trainFromC13Field)
    {/***edited ****/
      std::cout << "s-2" << std::endl;
      int borderList[2] = {26, 28};
      int avoidList[1] = {52};
      int includeList[4] = {17,19,21,23};
      trainFromC13Road = detectRange(minPoint, maxPoint, borderList, 2, 30, sizeof(areaArray) / sizeof(areaArray[0]), avoidList, 1, includeList, 4);
    }
    else
    {  /***edited ****/
      avoidStart = true;
      std::cout << "s-1" << std::endl;
      int borderList[2] = {12, 14};
      int avoidList[] = {52};
      int includeList[] = {};
      trainFromC13Field = detectRange(minPoint, maxPoint, borderList, 2, 1, sizeof(areaArray) / sizeof(areaArray[0]), avoidList, 1, includeList, 0);
    }
  }
  else if (!trainFromC13 && trainFromC14)
  { // Only Train coming from C14
    std::cout << "From C14 Only" << std::endl;
    if (trainFromC14End)
    {
      std::cout << "s-4" << std::endl;
    }
    else if (trainFromC14Road)
    { 
      std::cout << "s-3" << std::endl;
      int borderList[] = {11, 13};
      int avoidList[1] = {51};
      int includeList[1] = {0};
      trainFromC14End = detectRange(minPoint, maxPoint, borderList, 2, 1, 10, avoidList, 1, includeList, 0);
    }
    else if (trainFromC14Field)
    {
      std::cout << "s-2" << std::endl;
      int borderList[2] = {23, 25};
      int avoidList[] = {};
      int includeList[2] = {51, 52};
      trainFromC14Road = detectRange(minPoint, maxPoint, borderList, 2, 1, 22, avoidList, 0, includeList, 2);
    }
    else
    {
      std::cout << "s-1" << std::endl;
      avoidStart = true;
      avoidAreaList[44] = 1;
      avoidAreaList[46] = 1;
      avoidAreaList[48] = 1;
      int borderList[2] = {40, 43};
      int avoidList[] = {40, 43, 45, 47, 49};
      int includeList[] = {};
      trainFromC14Field = detectRange(minPoint, maxPoint, borderList, 2, 1, 62, avoidList, 5, includeList, 0);
    }
  }
  else if (trainFromC13 && trainFromC14)
  { //  Train coming from C13 and C14
    std::cout << "From Both" << std::endl;
    if (trainFromC13End)
    { // C13 is leaving the detecting area 41,44
      if (trainFromC14End)
      { // C14 is leaving the detecting area
      }
      else if (trainFromC14Road)
      { // 13
        int avoidList[1] = {0};
        int includeList[] = {1, 2, 3, 4, 5, 7, 9};
        int borderList14[] = {11, 13};
        trainFromC14End = detectRange(minPoint, maxPoint, borderList14, 2, 0, 0, avoidList, 0, includeList, 7);
      }
      else if (trainFromC14Field)
      { //25
        int avoidList[1] = {0};
        int includeList[] = {1, 2, 3, 4, 5, 7, 9, 11, 13, 51, 15, 17, 19, 21, 23};
        int borderList14[1] = {25};
        avoidStart = false;
        trainFromC14Road = detectRange(minPoint, maxPoint, borderList14, 1, 0, 0, avoidList, 0, includeList, 15);
      }
      else
      { // 40 43
        int avoidList[1] = {0};
        int includeList[16] = {1, 3, 5, 7, 9, 11, 13, 51, 15, 17, 19, 21, 27, 29};
        int borderList14[2] = {40, 43};
        avoidStart = false;
        trainFromC14Field = detectRange(minPoint, maxPoint, borderList14, 2, 1, 6, avoidList, 0, includeList, 14);
      }
    }
    else if (trainFromC13Road)
    { // C13 is leaving the road area 26,28
      if (trainFromC14End)
      { // 41 42 44
        int avoidList[1] = {0};
        int includeList[2] = {48, 50};
        int borderList13[3] = {41, 42, 44};
        trainFromC13End = detectRange(minPoint, maxPoint, borderList13, 3, 0, 0, avoidList, 0, includeList, 2);
      }
      else if (trainFromC14Road)
      { // 13, 41 42 44
        int avoidList[1] = {0};
        int includeList[8] = {1, 3, 5, 7, 9, 51, 48, 50};
        int borderList14[] = {11, 13};
        trainFromC14End = detectRange(minPoint, maxPoint, borderList14, 2, 0, 0, avoidList, 0, includeList, 8);
        int borderList13[3] = {41, 42, 44};
        trainFromC13End = detectRange(minPoint, maxPoint, borderList13, 3, 0, 0, avoidList, 0, includeList, 0);
      }
      else if (trainFromC14Field)
      { // 25 , 41 42 44
        int avoidList[1] = {0};
        int includeList[12] = {1, 3, 5, 7, 9, 11, 13, 51, 15, 17, 19, 21};
        int borderList14[1] = {25};
        trainFromC14Road = detectRange(minPoint, maxPoint, borderList14, 1, 0, 0, avoidList, 0, includeList, 12);
        int borderList13[3] = {41, 42, 44};
        trainFromC13End = detectRange(minPoint, maxPoint, borderList13, 3, 0, 0, avoidList, 0, includeList, 0);
      }
      else
      { //40 43, 41 42 44
        int avoidList[1] = {0};
        int includeList[13] = {1, 3, 5, 7, 9, 11, 13, 51, 15, 17, 19, 27, 29};
        int borderList14[3] = {40, 43};
        trainFromC14Field = detectRange(minPoint, maxPoint, borderList14, 2, 0, 0, avoidList, 0, includeList, 13);
        int borderList13[3] = {41, 42, 44};
        trainFromC13End = detectRange(minPoint, maxPoint, borderList13, 3, 0, 0, avoidList, 0, includeList, 0);
      }
    }
    else if (trainFromC13Field)
    { // C13 is leaving the Field area 12,14
      if (trainFromC14End)
      { //26 28
        int avoidList[2] = {51, 52};
        int includeList[] = {53, 54, 55, 56, 57, 58, 59, 60, 61, 62};
        int borderList13[2] = {26, 28};
        trainFromC13Road = detectRange(minPoint, maxPoint, borderList13, 2, 32, sizeof(areaArray) / sizeof(areaArray[0]), avoidList, 2, includeList, 10);
      }
      else if (trainFromC14Road)
      { // 13, 26 28
        int avoidList[1] = {0};
        int includeList[25] = {30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 42, 44, 46, 48, 50};
        int borderList14[] = {11, 13};
        trainFromC14End = detectRange(minPoint, maxPoint, borderList14, 2, 0, 0, avoidList, 0, includeList, 25);
        int borderList13[2] = {26, 28};
        trainFromC13Road = detectRange(minPoint, maxPoint, borderList13, 2, 0, 0, avoidList, 0, includeList, 0);
      }
      else if (trainFromC14Field)
      { //23 25,26 28
        int avoidList[1] = {0};
        int includeList[31] = {1, 3, 5, 7, 9, 11, 13, 51, 15, 17, 19, 21, 31, 32, 33, 34, 35, 36, 37, 38, 39, 53, 34, 55, 53, 57, 58, 59, 60, 61, 62};
        int borderList14[2] = {23, 25};
        trainFromC14Road = detectRange(minPoint, maxPoint, borderList14, 2, 0, 0, avoidList, 0, includeList, 31);
        int borderList13[2] = {26, 28};
        trainFromC13Road = detectRange(minPoint, maxPoint, borderList13, 2, 0, 0, avoidList, 0, includeList, 0);
      }
      else
      { // 40 43 ,26 28
        int avoidList[1] = {0};
        int includeList[31] = {1, 3, 5, 7, 9, 11, 13, 51, 15, 17, 19, 21, 31, 32, 33, 34, 35, 36, 37, 38, 39, 53, 34, 55, 53, 57, 58, 59, 60, 61, 62};
        int borderList14[] = {40, 43};
        trainFromC14Field = detectRange(minPoint, maxPoint, borderList14, 2, 0, 0, avoidList, 0, includeList, 31);
        int borderList13[2] = {26, 28};
        trainFromC13Road = detectRange(minPoint, maxPoint, borderList13, 2, 0, 0, avoidList, 0, includeList, 31);
      }
    }
    else
    {
      if (trainFromC14End)
      { // 12 14
        int avoidList[2] = {51, 52};
        int borderList13[2] = {12, 14};
        int includeList[16] = {18, 20, 22, 24, 26, 28, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39};
        trainFromC13Field = detectRange(minPoint, maxPoint, borderList13, 2, 43, sizeof(areaArray) / sizeof(areaArray[0]), avoidList, 2, includeList, 16);
      }
      else if (trainFromC14Road)
      { // 13, 12 14
        int avoidList[1] = {0};
        int includeList[26] = {18, 20, 22, 24, 26, 28, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 53, 54, 55, 56, 57, 58, 59, 60, 60, 62};
        int borderList14[] = {11, 13};
        trainFromC14End = detectRange(minPoint, maxPoint, borderList14, 2, 0, 0, avoidList, 0, includeList, 26);
        int borderList13[] = {12, 14};
        trainFromC13Field = detectRange(minPoint, maxPoint, borderList13, 2, 0, 0, avoidList, 0, includeList, 0);
      }
      else if (trainFromC14Field)
      { // 25 ,12 14
        int avoidList[1] = {0};
        int includeList[1] = {0};
        int borderList14[1] = {25};
        trainFromC14Road = detectRange(minPoint, maxPoint, borderList14, 1, 15, 22, avoidList, 0, includeList, 0);
        int borderList13[4] = {12, 14};
        trainFromC13Field = detectRange(minPoint, maxPoint, borderList13, 2, 0, 0, avoidList, 0, includeList, 0);
      }
      else
      { // 40 43 , 12 14
        int avoidList[1] = {52};
        int includeList[13] = {44, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62};
        int borderList14[2] = {40, 43};
        trainFromC14Field = detectRange(minPoint, maxPoint, borderList14, 2, 17, 39, avoidList, 1, includeList, 13);
        int borderList13[2] = {12, 14};
        trainFromC13Field = detectRange(minPoint, maxPoint, borderList13, 2, 0, 0, avoidList, 1, includeList, 0);
      }
    }
  }
  // else
  //   detectWhole( minPoint, maxPoint);
}

void detectArea(ProcessPointClouds<pcl::PointXYZI> *pointProcessorI, const pcl::PointCloud<pcl::PointXYZI>::Ptr &inputCloud, CURL *curl)
{
  pcl::PassThrough<pcl::PointXYZI> pass;
  pass.setInputCloud(inputCloud);
  pass.setFilterFieldName("z");
  pass.setFilterLimits(minZArea, maxZArea);
  pass.filter(*inputCloud);
  pcl::PointCloud<pcl::PointXYZI>::Ptr detectedArea(new pcl::PointCloud<pcl::PointXYZI>);
  detectedArea = getDetectAreaDangerous(inputCloud);

  if (viewFull == 1)
  {
    renderPointCloud(viewer, inputCloud, "viewFull");
  }
  else
  {
    renderPointCloud(viewer, detectedArea, "filterCloud");
  }
  if (enableDetect == 1)
  {
    if (enableView == 1)
      plotDetectAreaDangerous();

    if (detectedArea->size() > 0)
    {
      std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> cloudClusters = pointProcessorI->Clustering(detectedArea, boxClusterTolerance, boxMinSize, boxMaxSize);
      int clusterId = 0;
      for (pcl::PointCloud<pcl::PointXYZI>::Ptr cluster : cloudClusters)
      {
        pcl::PointXYZI minPoint, maxPoint;
        pcl::getMinMax3D(*cluster, minPoint, maxPoint);
        plot = 0;
        //detect train
        if (avoidLRT == 1)
          avoidLRTCheck(minPoint, maxPoint);
        else
          detectWhole(minPoint, maxPoint);
        if (plot == 1)
          plotBox(cluster, clusterId);
        ++clusterId;
      }
    }
    if (enableDB == 1)
    {
      if (updateRStart == 1)
      {
        updateDBcount = updateDBcount - 1;
        if (enablePCDSave == 1)
        {
          time_t my_time = time(NULL);
          std::string path = "/home/chiper/Desktop/Restriction/" + std::string(ctime(&my_time)) + ".pcd"; // ok
          pointProcessorI->savePcd(inputCloud, path);
        }
        if (updateDBcount <= 0)
        {
          updateRStart = 0;
        //  dbUpdate(restictionArea, 1, curl);

          memset(restictionArea, 0, sizeof(restictionArea)); // for automatically-allocated arrays
          resetCount = resetTime;
          updateDBcount = 10;
        }
      }

      if (resetCountStart == 1 && updateRStart == 0)
      {
        resetCount--;
        if (resetCount <= 0)
        {
          if (enableMsg == 1)
            std::cout << "RESET" << std::endl;
          resetCountStart = 0;
          resetCount = resetTime;
          memset(restictionArea, 0, sizeof(restictionArea)); // for automatically-allocated arrays
        //  dbUpdate(restictionArea, 1, curl);
        }
      }
    }
  }
}

void initCamera(CameraAngle setAngle)
{
  viewer->setBackgroundColor(0, 0, 0);
  viewer->addCoordinateSystem(1.0);
}

void display_a()
{
  pcl::transformPointCloud(*clouda_ptr, *clouda_ptr_trans, init_guess); //transform and rotate lidar A with initial guess  -> output -> clouda_ptr_trans
  sensor_msgs::PointCloud2 cloud_res;
  pcl::toROSMsg(*clouda_ptr_trans, cloud_res);
  pub_a.publish(cloud_res);
}
void display_b()
{
  sensor_msgs::PointCloud2 cloud_res;
  pcl::toROSMsg(*cloudb_ptr, cloud_res);
  pub_b.publish(cloud_res);
}

void cloud_a(const sensor_msgs::PointCloud2ConstPtr &input)
{
  checkShutDown = 10;
  pcl::fromROSMsg(*input.get(), *clouda_ptr);
  if (mode == 1)
  {
    display_a();
  }
}
void cloud_b(const sensor_msgs::PointCloud2ConstPtr &input)
{

  pcl::fromROSMsg(*input.get(), *cloudb_ptr);
  checkShutDown = 10;

  pcl::PointCloud<pcl::PointXYZI>::Ptr clouda_final_ptr(new pcl::PointCloud<pcl::PointXYZI>);
  pcl::transformPointCloud(*clouda_ptr, *clouda_final_ptr, manualMatrix); //transform lidar A directly without using icp
  *cloudc_ptr = *clouda_final_ptr + *cloudb_ptr;                          // combine lidar a and lidar b together after ICP -> output -> cloudc_ptr
  sensor_msgs::PointCloud2 cloud_res;
  pcl::toROSMsg(*cloudc_ptr, cloud_res);
  viewer->removeAllPointClouds();
  viewer->removeAllShapes();
  detectArea(pointProcessorI, cloudc_ptr, curl);
  viewer->spinOnce();
}

void assignArea()
{
  areaArray[0] = Area(xminR1, xmaxR1, yminR1, ymaxR1, zminR1, zmaxR1);
  areaArray[1] = Area(xminR2, xmaxR2, yminR2, ymaxR2, zminR2, zmaxR2);
  areaArray[2] = Area(xminR3, xmaxR3, yminR3, ymaxR3, zminR3, zmaxR3);
  areaArray[3] = Area(xminR4, xmaxR4, yminR4, ymaxR4, zminR4, zmaxR4);
  areaArray[4] = Area(xminR5, xmaxR5, yminR5, ymaxR5, zminR5, zmaxR5);
  areaArray[5] = Area(xminR6, xmaxR6, yminR6, ymaxR6, zminR6, zmaxR6);
  areaArray[6] = Area(xminR7, xmaxR7, yminR7, ymaxR7, zminR7, zmaxR7);
  areaArray[7] = Area(xminR8, xmaxR8, yminR8, ymaxR8, zminR8, zmaxR8);
  areaArray[8] = Area(xminR9, xmaxR9, yminR9, ymaxR9, zminR9, zmaxR9);
  areaArray[9] = Area(xminR10, xmaxR10, yminR10, ymaxR10, zminR10, zmaxR10);
  areaArray[10] = Area(xminR11, xmaxR11, yminR11, ymaxR11, zminR11, zmaxR11);
  areaArray[11] = Area(xminR12, xmaxR12, yminR12, ymaxR12, zminR12, zmaxR12);
  areaArray[12] = Area(xminR13, xmaxR13, yminR13, ymaxR13, zminR13, zmaxR13);
  areaArray[13] = Area(xminR14, xmaxR14, yminR14, ymaxR14, zminR14, zmaxR14);
  areaArray[14] = Area(xminR15, xmaxR15, yminR15, ymaxR15, zminR15, zmaxR15);
  areaArray[15] = Area(xminR16, xmaxR16, yminR16, ymaxR16, zminR16, zmaxR16);
  areaArray[16] = Area(xminR17, xmaxR17, yminR17, ymaxR17, zminR17, zmaxR17);
  areaArray[17] = Area(xminR18, xmaxR18, yminR18, ymaxR18, zminR18, zmaxR18);
  areaArray[18] = Area(xminR19, xmaxR19, yminR19, ymaxR19, zminR19, zmaxR19);
  areaArray[19] = Area(xminR20, xmaxR20, yminR20, ymaxR20, zminR20, zmaxR20);
  areaArray[20] = Area(xminR21, xmaxR21, yminR21, ymaxR21, zminR21, zmaxR21);
  areaArray[21] = Area(xminR22, xmaxR22, yminR22, ymaxR22, zminR22, zmaxR22);
  areaArray[22] = Area(xminR23, xmaxR23, yminR23, ymaxR23, zminR23, zmaxR23);
  areaArray[23] = Area(xminR24, xmaxR24, yminR24, ymaxR24, zminR24, zmaxR24);
  areaArray[24] = Area(xminR25, xmaxR25, yminR25, ymaxR25, zminR25, zmaxR25);
  areaArray[25] = Area(xminR26, xmaxR26, yminR26, ymaxR26, zminR26, zmaxR26);
  areaArray[26] = Area(xminR27, xmaxR27, yminR27, ymaxR27, zminR27, zmaxR27);
  areaArray[27] = Area(xminR28, xmaxR28, yminR28, ymaxR28, zminR28, zmaxR28);
  areaArray[28] = Area(xminR29, xmaxR29, yminR29, ymaxR29, zminR29, zmaxR29);
  areaArray[29] = Area(xminR30, xmaxR30, yminR30, ymaxR30, zminR30, zmaxR30);
  areaArray[30] = Area(xminR31, xmaxR31, yminR31, ymaxR31, zminR31, zmaxR31);
  areaArray[31] = Area(xminR32, xmaxR32, yminR32, ymaxR32, zminR32, zmaxR32);
  areaArray[32] = Area(xminR33, xmaxR33, yminR33, ymaxR33, zminR33, zmaxR33);
  areaArray[33] = Area(xminR34, xmaxR34, yminR34, ymaxR34, zminR34, zmaxR34);
  areaArray[34] = Area(xminR35, xmaxR35, yminR35, ymaxR35, zminR35, zmaxR35);
  areaArray[35] = Area(xminR36, xmaxR36, yminR36, ymaxR36, zminR36, zmaxR36);
  areaArray[36] = Area(xminR37, xmaxR37, yminR37, ymaxR37, zminR37, zmaxR37);
  areaArray[37] = Area(xminR38, xmaxR38, yminR38, ymaxR38, zminR38, zmaxR38);
  areaArray[38] = Area(xminR39, xmaxR39, yminR39, ymaxR39, zminR39, zmaxR39);
  areaArray[39] = Area(xminR40, xmaxR40, yminR40, ymaxR40, zminR40, zmaxR40);
  areaArray[40] = Area(xminR41, xmaxR41, yminR41, ymaxR41, zminR41, zmaxR41);
  areaArray[41] = Area(xminR42, xmaxR42, yminR42, ymaxR42, zminR42, zmaxR42);
  areaArray[42] = Area(xminR43, xmaxR43, yminR43, ymaxR43, zminR43, zmaxR43);
  areaArray[43] = Area(xminR44, xmaxR44, yminR44, ymaxR44, zminR44, zmaxR44);
  areaArray[44] = Area(xminR45, xmaxR45, yminR45, ymaxR45, zminR45, zmaxR45);
  areaArray[45] = Area(xminR46, xmaxR46, yminR46, ymaxR46, zminR46, zmaxR46);
  areaArray[46] = Area(xminR47, xmaxR47, yminR47, ymaxR47, zminR47, zmaxR47);
  areaArray[47] = Area(xminR48, xmaxR48, yminR48, ymaxR48, zminR48, zmaxR48);
  areaArray[48] = Area(xminR49, xmaxR49, yminR49, ymaxR49, zminR49, zmaxR49);
  areaArray[49] = Area(xminR50, xmaxR50, yminR50, ymaxR50, zminR50, zmaxR50);
  areaArray[50] = Area(xminR51, xmaxR51, yminR51, ymaxR51, zminR51, zmaxR51);
  areaArray[51] = Area(xminR52, xmaxR52, yminR52, ymaxR52, zminR52, zmaxR52);
  areaArray[52] = Area(xminR53, xmaxR53, yminR53, ymaxR53, zminR53, zmaxR53);
  areaArray[53] = Area(xminR54, xmaxR54, yminR54, ymaxR54, zminR54, zmaxR54);
  areaArray[54] = Area(xminR55, xmaxR55, yminR55, ymaxR55, zminR55, zmaxR55);
  areaArray[55] = Area(xminR56, xmaxR56, yminR56, ymaxR56, zminR56, zmaxR56);
  areaArray[56] = Area(xminR57, xmaxR57, yminR57, ymaxR57, zminR57, zmaxR57);
  areaArray[57] = Area(xminR58, xmaxR58, yminR58, ymaxR58, zminR58, zmaxR58);
  areaArray[58] = Area(xminR59, xmaxR59, yminR59, ymaxR59, zminR59, zmaxR59);
  areaArray[59] = Area(xminR60, xmaxR60, yminR60, ymaxR60, zminR60, zmaxR60);
  areaArray[60] = Area(xminR61, xmaxR61, yminR61, ymaxR61, zminR61, zmaxR61);
  areaArray[61] = Area(xminR62, xmaxR62, yminR62, ymaxR62, zminR62, zmaxR62);
  areaArray[62] = Area(xminR63, xmaxR63, yminR63, ymaxR63, zminR63, zmaxR63);
  areaArray[63] = Area(xminR64, xmaxR64, yminR64, ymaxR64, zminR64, zmaxR64);
  areaArray[64] = Area(xminR65, xmaxR65, yminR65, ymaxR65, zminR65, zmaxR65);
}

void checkVelodyneSignal(const ros::TimerEvent &)
{
  if (checkShutDown >= 0)
    checkShutDown--;
  if (checkShutDown <= 0)
  {
    //  ROS_INFO("Waiting the next train... Reseted");
    trainFromC13 = false;
    trainFromC13Field = false;
    trainFromC13Road = false;
    trainFromC13End = false;
    trainFromC14 = false;
    trainFromC14Field = false;
    trainFromC14Road = false;
    trainFromC14End = false;
    d1AStatus = 0;
    d2AStatus = 0;
    d3AStatus = 0;
    d4AStatus = 0;
    d5AStatus = 0;
    d6AStatus = 0;
    d1DStatus = 0;
    d2DStatus = 0;
    d3DStatus = 0;
    d4DStatus = 0;
    d5DStatus = 0;
    d6DStatus = 0;
  }
}

int main(int argc, char **argv)
{

  // loadConfig();
  // assignArea();
  // for (int i = 0; i < 65; i++)
  //   restictionArea[i] = 0;
  // curl_global_init(CURL_GLOBAL_ALL);
  // curl = curl_easy_init();
  // std::cout << "starting enviroment" << std::endl;
  // CameraAngle setAngle = XY;
  // if (enableView == 1)
  //   initCamera(setAngle);
  // //  manualMatrix <<-0.577224 ,0.81658 ,-0.00315049 ,23.7078, -0.816584,-0.577227,-0.000191713,24.1589,-0.0019751 , 0.00246198 ,0.999995 , -0.0224675,0,0,0,1;
  // // manualMatrix <<-0.578092 ,0.815971 ,-0.00315049 ,23.9322, -0.816584,-0.577227,-0.000191713,24.1589,-0.0019751 , 0.00246198 ,0.999995 , -0.0224675,0,0,0,1;
  // Eigen::AngleAxisf init_rotation_x(rotate_x, Eigen::Vector3f::UnitX());
  // Eigen::AngleAxisf init_rotation_y(rotate_y, Eigen::Vector3f::UnitY());
  // Eigen::AngleAxisf init_rotation_z(rotate_z, Eigen::Vector3f::UnitZ());
  // Eigen::Translation3f init_translation(tran_x, tran_y, tran_z);

  // manualMatrix = (init_rotation_x * init_rotation_y * init_rotation_z * init_translation).matrix();
  // // initialize Ros node
  // ros::init(argc, argv, "my_pcl_tutorial");
  // ros::NodeHandle nh;
  // // subscribe lidar a
  // ros::Subscriber sub1 = nh.subscribe("/ns1/velodyne_points", 10, cloud_a);
  // // subscribe lidar b
  // ros::Subscriber sub2 = nh.subscribe("/ns2/velodyne_points", 10, cloud_b);
  // std::cout << "out" << std::endl;
  // pub_a = nh.advertise<sensor_msgs::PointCloud2>("pc_a", 10);
  // pub_b = nh.advertise<sensor_msgs::PointCloud2>("pc_b", 10);
  // pub_res = nh.advertise<sensor_msgs::PointCloud2>("pub_res", 10);
  // initCamera(setAngle);
  // ros::Timer timer1 = nh.createTimer(ros::Duration(2.0), checkVelodyneSignal);
  // ros::spin();

  Eigen::Matrix4f init_guess;

  loadConfig();
  assignArea();
  for (int i = 0; i < 65; i++)
    restictionArea[i] = 0;
  curl_global_init(CURL_GLOBAL_ALL);
  curl = curl_easy_init();
  Eigen::AngleAxisf init_rotation_x(rotate_x, Eigen::Vector3f::UnitX());
  Eigen::AngleAxisf init_rotation_y(rotate_y, Eigen::Vector3f::UnitY());
  Eigen::AngleAxisf init_rotation_z(rotate_z, Eigen::Vector3f::UnitZ());
  Eigen::Translation3f init_translation(tran_x, tran_y, tran_z);

  init_guess = (init_rotation_x * init_rotation_y * init_rotation_z * init_translation).matrix();

  std::cout << init_guess << std::endl;

  std::cout << "starting enviroment" << std::endl;
  CameraAngle setAngle = XY;
  if (enableView == 1)
    initCamera(setAngle);

  //std::vector<boost::filesystem::path> stream1 = pointProcessorI->streamPcd("/home/weitinglo/Desktop/ns1");
  std::vector<boost::filesystem::path> stream2 = pointProcessorI->streamPcd("/home/weitinglo/Desktop/lrt");
  //auto streamIterator1 = stream1.begin();
  auto streamIterator2 = stream2.begin();
  pcl::PointCloud<pcl::PointXYZI>::Ptr combine(new pcl::PointCloud<pcl::PointXYZI>);

  // pcl::PointCloud<pcl::PointXYZI>::Ptr inputCloudI1;
  pcl::PointCloud<pcl::PointXYZI>::Ptr inputCloudI2;

  pcl::PointCloud<pcl::PointXYZI>::Ptr clouda_ptr_trans(new pcl::PointCloud<pcl::PointXYZI>);

  while (!viewer->wasStopped())
  {
    viewer->removeAllPointClouds();
    viewer->removeAllShapes();
    //  inputCloudI1 = pointProcessorI->loadPcd((*streamIterator1).string());
    inputCloudI2 = pointProcessorI->loadPcd((*streamIterator2).string());
    //streamIterator1++;
    streamIterator2++;
    //    pcl::transformPointCloud (*inputCloudI1, *clouda_ptr_trans, init_guess);
    // *combine=*clouda_ptr_trans+*inputCloudI2;
    detectArea(pointProcessorI, inputCloudI2, curl);
    renderPointCloud(viewer, inputCloudI2, "full view");
    //      renderPointCloud(viewer,inputCloudI2,"2",Color(1,0,0));
    // if(streamIterator1 == stream1.end()){
    //    streamIterator1 = stream1.begin();
    // }
    if (streamIterator2 == stream2.end())
    {
      trainFromC13 = false;
      trainFromC13Field = false;
      trainFromC13Road = false;
      trainFromC13End = false;
      trainFromC14 = false;
      trainFromC14Field = false;
      trainFromC14Road = false;
      trainFromC14End = false;
      d1AStatus = 0;
      d2AStatus = 0;
      d3AStatus = 0;
      d4AStatus = 0;
      d5AStatus = 0;
      d6AStatus = 0;
      d1DStatus = 0;
      d2DStatus = 0;
      d3DStatus = 0;
      d4DStatus = 0;
      d5DStatus = 0;
      d6DStatus = 0;
      streamIterator2 = stream2.begin();
    }
    viewer->spinOnce();
  }
}
