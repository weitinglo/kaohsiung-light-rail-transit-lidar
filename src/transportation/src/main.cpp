/* \author Wei-Ting Lo */
#include "sensors/lidar.h"
#include "processPointClouds.h"
#include "processPointClouds.cpp"
#include <ros/ros.h>
// PCL
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/conversions.h>
#include <pcl/registration/ndt.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/octree/octree_pointcloud_changedetector.h>
#include <pcl/filters/approximate_voxel_grid.h>

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/progressive_morphological_filter.h>
#include <pcl/segmentation/segment_differences.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>


ProcessPointClouds<pcl::PointXYZI> *pointProcessorI = new ProcessPointClouds<pcl::PointXYZI>();
pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
pcl::PointCloud<pcl::PointXYZI>::Ptr inputCloudI2;

float inner_cube_x_min = 0;
float inner_cube_x_max = 0;
float inner_cube_y_min = 0;
float inner_cube_y_max = 0;
float inner_cube_z_min = 0;
float inner_cube_z_max = 0;
double inner_cube_r = 1.0;
double inner_cube_g = 1.0;
double inner_cube_b = 1.0;

float clusterTolerance = 2;
int boxMinSize = 0;
int boxMaxSize = 0;

/*** background ***/
pcl::PointCloud<pcl::PointXYZI>::Ptr base(new pcl::PointCloud<pcl::PointXYZI>);
int arrayIndex = 0;
int removeBackgroundYN = 0;
float removeBackgroundDistance = 0.0004;
int removeBackgroundBaseTime = 10;

int loadConfig()
{
  std::ifstream cFile("/home/weitinglo/catkin_ws/src/transportation/src/config.txt");

  if (cFile.is_open())
  {
    std::string line;
    while (getline(cFile, line))
    {
      line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
      if (line[0] == '*' || line.empty())
        continue;
      auto delimiterPos = line.find("=");
      auto name = line.substr(0, delimiterPos);
      auto value = line.substr(delimiterPos + 1);
      std::cout << name << " " << value << '\n';
      if (name == "inner_cube_x_min")
      {
        inner_cube_x_min = atof(value.c_str());
      }
      else if (name == "inner_cube_x_max")
      {
        inner_cube_x_max = atof(value.c_str());
      }
      else if (name == "inner_cube_y_min")
      {
        inner_cube_y_min = atoi(value.c_str());
      }
      else if (name == "inner_cube_y_max")
      {
        inner_cube_y_max = atof(value.c_str());
      }
      else if (name == "inner_cube_z_min")
      {
        inner_cube_z_min = atoi(value.c_str());
      }
      else if (name == "inner_cube_z_max")
      {
        inner_cube_z_max = atof(value.c_str());
      }
      else if (name == "inner_cube_r")
      {
        inner_cube_r = atoi(value.c_str());
      }
      else if (name == "inner_cube_g")
      {
        inner_cube_g = atoi(value.c_str());
      }
      else if (name == "inner_cube_b")
      {
        inner_cube_b = atoi(value.c_str());
      }
      else if (name == "clusterTolerance")
      {
        clusterTolerance = atoi(value.c_str());
      }
      else if (name == "boxMinSize")
      {
        boxMinSize = atoi(value.c_str());
      }
      else if (name == "boxMaxSize")
      {
        boxMaxSize = atoi(value.c_str());
      }
      else if (name == "removeBackgroundYN")
      {
        removeBackgroundYN = atoi(value.c_str());
      }
      else if (name == "removeBackgroundDistance")
      {
        removeBackgroundDistance = atof(value.c_str());
      }
      else if (name == "removeBackgroundBaseTime")
      {
        removeBackgroundBaseTime = atoi(value.c_str());
      }
    }
  }
  else
  {
    std::cerr << "Couldn't open config file for reading.\n";
  }
}

void drawSelfArea()
{
  pcl::PointXYZ p1, p2, p3, p4;
  p1.x = inner_cube_x_min;
  p1.y = inner_cube_y_min;
  p1.z = -1;
  p2.x = inner_cube_x_max;
  p2.y = inner_cube_y_min;
  p2.z = -1;
  viewer->addLine(p1, p2, 0, 1, 0, "bottom", 0);
  p3.x = inner_cube_x_max;
  p3.y = inner_cube_y_max;
  p3.z = -1;
  viewer->addLine(p2, p3, 0, 1, 0, "right", 0);
  p4.x = inner_cube_x_min;
  p4.y = inner_cube_y_max;
  p4.z = -1;
  viewer->addLine(p3, p4, 0, 1, 0, "top", 0);
  viewer->addLine(p4, p1, 0, 1, 0, "left", 0);
}
void removeSelf(const pcl::PointCloud<pcl::PointXYZI>::Ptr &inputCloud)
{
  pcl::CropBox<pcl::PointXYZI> boxFilter;
  boxFilter.setMin(Eigen::Vector4f(inner_cube_x_min, inner_cube_y_min, -20, 1.0));
  boxFilter.setMax(Eigen::Vector4f(inner_cube_x_max, inner_cube_y_max, 20, 1.0));
  boxFilter.setInputCloud(inputCloud);
  boxFilter.setNegative(true);
  boxFilter.filter(*inputCloud);
}
void plotBox(pcl::PointCloud<pcl::PointXYZI>::Ptr cluster, int clusterId)
{
  Color boxColor = Color(1, 0, 0);
  Box box = pointProcessorI->BoundingBox(cluster);
  renderBox(viewer, box, clusterId, boxColor);
}
void detectArea(ProcessPointClouds<pcl::PointXYZI> *pointProcessorI, const pcl::PointCloud<pcl::PointXYZI>::Ptr &inputCloud)
{
  std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> cloudClusters = pointProcessorI->Clustering(inputCloud, clusterTolerance, boxMinSize, boxMaxSize);
  int clusterId = 0;
  for (pcl::PointCloud<pcl::PointXYZI>::Ptr cluster : cloudClusters)
  {
    plotBox(cluster, clusterId);
    ++clusterId;
  }
}
void setupCamera()
{
  viewer->setBackgroundColor(0, 0, 0);
  viewer->addCoordinateSystem(1.0);
  viewer->initCameraParameters();
  viewer->setCameraPosition(0, 5, 30, 0, 0, 0, 0, 0, 0);
  viewer->setCameraFieldOfView(0.523599);
  viewer->setCameraClipDistances(0.00522511, 50);
}

void removeBackground(const pcl::PointCloud<pcl::PointXYZI>::Ptr &inputCloud)
{
  
   if(arrayIndex<removeBackgroundBaseTime){
      *base= *base+*inputCloudI2;
      arrayIndex++;
    }else{
      pcl::SegmentDifferences<pcl::PointXYZI> seg; 
      seg.setDistanceThreshold(removeBackgroundDistance);
      seg.setInputCloud(base);
      seg.setTargetCloud(inputCloudI2);
      seg.segment(*inputCloudI2);
      pcl::StatisticalOutlierRemoval<pcl::PointXYZI> sor;
      sor.setInputCloud (inputCloudI2);
      sor.setMeanK (50);
      sor.setStddevMulThresh (1.0);
      sor.filter (*inputCloudI2);
    }
}
int main(int argc, char **argv)
{
  loadConfig();
  setupCamera();
  ros::init(argc, argv, "my_pcl_tutorial");
  std::vector<boost::filesystem::path> stream2 = pointProcessorI->streamPcd("/home/weitinglo/Desktop/room");
  auto streamIterator2 = stream2.begin();
  arrayIndex = 0;
  while (!viewer->wasStopped())
  {
    viewer->removeAllPointClouds();
    viewer->removeAllShapes();
    inputCloudI2 = pointProcessorI->loadPcd((*streamIterator2).string());
     pcl::VoxelGrid<pcl::PointXYZI> sor;
    sor.setInputCloud (inputCloudI2);
    sor.setLeafSize (0.05f, 0.05f, 0.05f);
    sor.filter (*inputCloudI2);
    streamIterator2++;

    if(removeBackgroundYN==1)
      removeBackground(inputCloudI2);

    drawSelfArea();
    
    renderPointCloud(viewer, inputCloudI2, "full view");


    // if(arrayIndex>=removeBackgroundBaseTime){
    //    detectArea(pointProcessorI, inputCloudI2);
    //   renderPointCloud(viewer, inputCloudI2, "full view");
    // }
    if (streamIterator2 == stream2.end())
    {
      streamIterator2 = stream2.begin();
    }
    viewer->spinOnce();
  }
}
